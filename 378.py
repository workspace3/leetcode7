# -*- coding: utf-8 -*-
"""
Given a n x n matrix where each of the rows and columns are sorted in ascending order,
find the kth smallest element in the matrix.
Note that it is the kth smallest element in the sorted order, not the kth distinct element.
-----------------------------
Example:

matrix = [
   [ 1,  5,  9],
   [10, 11, 13],
   [12, 13, 15]
],
k = 8,

return 13.
"""


class Solution:
    def kthSmallest(self, matrix, k):
        n = len(matrix)

        #
        def smaller_than(x):
            i = n - 1
            cnt = 0
            while i >= 0:
                if x < matrix[i][0]:
                    i -= 1
                    continue
                elif x >= matrix[i][-1]:
                    cnt += len(matrix[i])
                    i -= 1
                    continue
                else:
                    left = 0
                    right = len(matrix[i])
                    found_index = None
                    while left < right:
                        mid = (left + right) // 2
                        if matrix[i][mid] > x:
                            right = mid
                        else:
                            if matrix[i][mid+1] > x:
                                found_index = mid + 1
                                break
                            else:
                                left = mid + 1
                    cnt += found_index
                    i -= 1
            return cnt
        #
        lo, hi = matrix[0][0], matrix[n - 1][n - 1]
        while lo < hi:
            mi = (hi - lo) // 2 + lo
            cnt = smaller_than(mi)
            if cnt < k:
                lo = mi + 1
            else:
                hi = mi
        return lo


# main #
import timeit
s = Solution()
s_t = timeit.default_timer()
matrix = [
   [1,  5,  9],
   [1, 111, 1300],
   [120, 130, 1500]
]
k = 8
print(s.kthSmallest(matrix, k))
e_t = timeit.default_timer()
print("time cost: {:.5f} seconds.".format(e_t - s_t))

