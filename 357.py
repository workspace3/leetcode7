# -*- coding: utf-8 -*-
"""
Given a non-negative integer n, count all numbers with unique digits, x, where 0 ≤ x < 10n.

Example:

Input: 2
Output: 91
Explanation: The answer should be the total numbers in the range of 0 ≤ x < 100,
             excluding 11,22,33,44,55,66,77,88,99
"""


class Solution:
    def countNumbersWithUniqueDigits(self, n: int) -> int:
        if 0 == n:
            return 1
        elif 1 == n:
            return 10
        else:
            dp = [0 for _ in range(n+1)]
            dp[0], dp[1] = 1, 10
            for i in range(2, n+1):
                curr_val = 9
                j = 0
                base = 9
                while j < i - 1:
                    curr_val *= base
                    base -= 1
                    j += 1
                dp[i] = dp[i-1] + curr_val
            return dp[-1]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = 3
    s_t = timeit.default_timer()
    print(s.countNumbersWithUniqueDigits(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))

