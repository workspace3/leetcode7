# -*- coding: utf-8 -*-
"""
Given a set of distinct positive integers, find the largest subset such that
 every pair (Si, Sj) of elements in this subset satisfies:
Si % Sj = 0 or Sj % Si = 0.
If there are multiple solutions, return any subset is fine.
-------------------------------------------
Example 1:

Input: [1,2,3]
Output: [1,2] (of course, [1,3] will also be ok)
Example 2:

Input: [1,2,4,8]
Output: [1,2,4,8]
"""


class Solution:
    def largestDivisibleSubset(self, nums):
        if len(nums) == 0:
            return []
        elif len(nums) == 1:
            return nums
        nums = sorted(nums)
        dp = {}
        result = [nums[0]]
        for i in range(len(nums)):
            if nums[i] not in dp:
                dp[nums[i]] = [nums[i]]
            for j in range(i):
                if nums[i] % nums[j] == 0:
                    dp[nums[i]] = dp[nums[j]] + [nums[i]]
                    if len(dp[nums[i]]) > len(result):
                        result = dp[nums[i]]
        return result

    def largestDivisibleSubset_from_others(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        if not nums:
            return []
        nums = sorted(nums)
        dp = [[nums[i]] for i in range(len(nums))]
        max_len, res = 1, dp[0]
        for i in range(1, len(nums)):
            for j in range(i):
                if nums[i] % nums[j] == 0 and len(dp[i]) < len(dp[j]) + 1:
                    dp[i] = dp[j] + [nums[i]]
                    if len(dp[i]) > max_len:
                        max_len, res = len(dp[i]), dp[i]
        return res


import timeit
s = Solution()
nums = [546,669]
print(len(nums))
s_t = timeit.default_timer()
print(s.largestDivisibleSubset(nums))
e_t = timeit.default_timer()
print("time cost: {:.5f} seconds.".format(e_t - s_t))

