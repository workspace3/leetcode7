"""
Given an array A of integers, for each integer A[i] we need to choose either x = -K or x = K,
and add x to A[i] (only once).

After this process, we have some array B.

Return the smallest possible difference between the maximum value of B and the minimum value of B.



Example 1:

Input: A = [1], K = 0
Output: 0
Explanation: B = [1]
Example 2:

Input: A = [0,10], K = 2
Output: 6
Explanation: B = [2,8]
Example 3:

Input: A = [1,3,6], K = 3
Output: 3
Explanation: B = [4,6,3]


Note:

1 <= A.length <= 10000
0 <= A[i] <= 10000
0 <= K <= 10000
------------------------------
他人思路及解法:
思路：
初步想法肯定把大的减小，把小的增大，最次也是同时增大或者同时减小，不存在把小的减小反而把大的增大，想到这一步就好做了
我们先将A排序，然后将若干个大的减小，剩下的小的增大，但是具体将多少个大的减小呢，这个我们不能确定，所以我们得依次遍历

class Solution(object):
    def smallestRangeII(self, A, K):
        A.sort()
        # 初始默认全部加k
        for i in range(len(A)): A[i] += K
        # 使用ma,mi保存当前的最大和最小值
        ma, mi = A[-1], A[0]
        res = ma - mi
        # M代表-k的那部分数中的最大值
        # m代表+k的那部分数中的最小值
        M,m = ma-2*K,mi
        for i in range(len(A)-1,0,-1):
            A[i] -= 2*K
            ma = max(M, A[i-1])
            mi = min(A[i], m)
            res = min(res, ma - mi)
        return res
"""


class Solution:
    def smallestRangeII(self, A, K) -> int:
        length = len(A)
        if 1 == length:
            return 0
        # pending
        return 0


if __name__ == "__main__":
    import timeit
    s = Solution()
    A = [1,3,6]
    K = 3
    s_t = timeit.default_timer()
    print(s.smallestRangeII(A, K))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))