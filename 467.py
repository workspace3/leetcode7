# -*- coding: utf-8 -*-
"""
Consider the string s to be the infinite wraparound string of "abcdefghijklmnopqrstuvwxyz",
so s will look like this: "...zabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcd....".

Now we have another string p. Your job is to find out how many unique non-empty substrings of p are present in s.
In particular, your input is the string p and you need to output the number of different non-empty substrings of p
in the string s.

Note: p consists of only lowercase English letters and the size of p might be over 10000.

Example 1:
Input: "a"
Output: 1

Explanation: Only the substring "a" of string "a" is in the string s.
Example 2:
Input: "cac"
Output: 2
Explanation: There are two substrings "a", "c" of string "cac" in the string s.
Example 3:
Input: "zab"
Output: 6
Explanation: There are six substrings "z", "a", "b", "za", "ab", "zab" of string "zab" in the string s.
"""


class Solution:
    def findSubstringInWraproundString(self, p: str) -> int:
        def is_next(a, b):
            if ord(a) - ord(b) == 1 or ("a" == a and "z" == b):
                return True
            else:
                return False
        #
        dp = [[0, 1] for _ in range(len(p))]
        for i in range(1, len(p)):
            if is_next(p[i], p[i-1]):
                dp[i][1] += dp[i-1][1]
            dp[i][0] += (dp[i-1][0] + dp[i-1][1])
        print(dp)
        return dp[-1][0] + dp[-1][1]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "cac"
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.findSubstringInWraproundString(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
