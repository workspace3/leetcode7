# -*- coding: utf-8 -*-
"""
Given a m x n matrix, if an element is 0, set its entire row and column to 0. Do it in-place.

Example 1:

Input:
[
  [1,1,1],
  [1,0,1],
  [1,1,1]
]
Output:
[
  [1,0,1],
  [0,0,0],
  [1,0,1]
]

Example 2:

Input:
[
  [0,1,2,0],
  [3,4,5,2],
  [1,3,1,5]
]
Output:
[
  [0,0,0,0],
  [0,4,5,0],
  [0,3,1,0]
]

"""
from typing import List


class Solution:
    def setZeroes(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        rows, cols = len(matrix), len(matrix[0])

        def _set_row_zero(r):
            matrix[r] = [0]*len(matrix[r])

        def _set_col_zero(c):
            for i in range(len(matrix)):
                matrix[i][c] = 0

        def _find_a_zero():
            for i in range(rows):
                for j in range(cols):
                    if 0 == matrix[i][j]:
                        return i, j
            return None
        #
        the_zero_pos = _find_a_zero()
        if the_zero_pos is None:
            pass
        else:
            the_r, the_c = the_zero_pos
            for r in range(rows):
                for c in range(cols):
                    if 0 == matrix[r][c]:
                        matrix[the_r][c] = 0
                        matrix[r][the_c] = 0
                    else:
                        pass
            #
            for c in range(cols):
                if c != the_c and 0 == matrix[the_r][c]:
                    _set_col_zero(c)
            #
            for r in range(rows):
                if r != the_r and 0 == matrix[r][the_c]:
                    _set_row_zero(r)
            #
            _set_col_zero(the_c)
            _set_row_zero(the_r)


if __name__ == "__main__":
    import timeit
    from pprint import pprint
    s = Solution()
    ss = [
  [1,1,1],
  [1,0,1],
  [1,1,1]
]
    print(len(ss))
    s_t = timeit.default_timer()
    s.setZeroes(ss)
    pprint(ss)
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
