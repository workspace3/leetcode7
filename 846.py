"""
Alice has a hand of cards, given as an array of integers.

Now she wants to rearrange the cards into groups so that each group is size W,
and consists of W consecutive cards.

Return true if and only if she can.


Example 1:

Input: hand = [1,2,3,6,2,3,4,7,8], W = 3
Output: true
Explanation: Alice's hand can be rearranged as [1,2,3],[2,3,4],[6,7,8].
Example 2:

Input: hand = [1,2,3,4,5], W = 4
Output: false
Explanation: Alice's hand can't be rearranged into groups of 4.


Note:

1 <= hand.length <= 10000
0 <= hand[i] <= 10^9

-------------------------
其他人解法：
    hand.sort()
    while hand:
        try:
            base = hand[0]
            for i in range(W):
                hand.remove(base+i)
        except:
            return False
    return True

"""


class Solution:
    def isNStraightHand(self, hand, W) -> bool:
        from collections import Counter
        length = len(hand)
        if length % W != 0:
            return False
        count = Counter(hand)
        while count:
            key = min(list(count.keys()))
            for next_key in range(key, key+W):
                if next_key in count:
                    count[next_key] -= 1
                    if 0 == count[next_key]:
                        del count[next_key]
                else:
                    return False
        return True


if __name__ == "__main__":
    import timeit
    s = Solution()
    hand = [1, 2, 3, 4, 5, 6]
    W = 2
    s_t = timeit.default_timer()
    print(s.isNStraightHand(hand, W))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
