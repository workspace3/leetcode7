# -*- coding: utf-8 -*-
"""
Given a 2D board containing 'X' and 'O' (the letter O), capture all regions surrounded by 'X'.

A region is captured by flipping all 'O's into 'X's in that surrounded region.

Example:

X X X X
X O O X
X X O X
X O X X

After running your function, the board should be:

X X X X
X X X X
X X X X
X O X X

Explanation:

Surrounded regions shouldn’t be on the border,
which means that any 'O' on the border of the board are not flipped to 'X'.
Any 'O' that is not on the border and it is not connected to an 'O' on the border will be flipped to 'X'.
Two cells are connected if they are adjacent cells connected horizontally or vertically.

"""
from typing import List


class Solution:
    def solve(self, board: List[List[str]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """
        if len(board) == 0 or len(board[0]) == 0:
            pass
        else:
            rows, cols = len(board), len(board[0])
            # 从边界向内搜索，把所有的与边界联通的O都替换为*
            for r in range(rows):
                for c in range(cols):
                    if not (0 == r or rows-1 == r or 0 == c or cols-1 == c):
                        continue
                    if "O" != board[r][c]:
                        continue
                    board[r][c] = "*"
                    stack = [(r, c)]
                    while stack:
                        curr_r, curr_c = stack.pop()
                        if 0 < curr_r-1 < rows-1 and "O" == board[curr_r-1][curr_c]:
                            stack.append((curr_r-1, curr_c))
                            board[curr_r-1][curr_c] = "*"
                        if 0 < curr_r+1 < rows-1 and "O" == board[curr_r+1][curr_c]:
                            stack.append((curr_r+1, curr_c))
                            board[curr_r+1][curr_c] = "*"
                        if 0 < curr_c + 1 < cols - 1 and "O" == board[curr_r][curr_c+1]:
                            stack.append((curr_r, curr_c+1))
                            board[curr_r][curr_c+1] = "*"
                        if 0 < curr_c - 1 < cols - 1 and "O" == board[curr_r][curr_c - 1]:
                            stack.append((curr_r, curr_c - 1))
                            board[curr_r][curr_c - 1] = "*"
            # 遍历所有元素，将所有的O都替换为X（被X包围的），将所有的*都替换为O（没有被X包围的）
            for r in range(rows):
                for c in range(cols):
                    if "O" == board[r][c]:
                        board[r][c] = "X"
                    elif "*" == board[r][c]:
                        board[r][c] = "O"


if __name__ == "__main__":
    import timeit
    from pprint import pprint
    s = Solution()
    ss = [["X", "X", "X", "X"],
          ["X", "X", "O", "X"],
          ["X", "O", "X", "O"],
          ["X", "X", "X", "X"]]
    print(len(ss))
    s_t = timeit.default_timer()
    s.solve(ss)
    pprint(ss)
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
