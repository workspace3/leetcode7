"""
Given a positive integer n,
break it into the sum of at least two positive integers and maximize the product of those integers.
Return the maximum product you can get.

Example 1:

Input: 2
Output: 1
Explanation: 2 = 1 + 1, 1 × 1 = 1.
Example 2:

Input: 10
Output: 36
Explanation: 10 = 3 + 3 + 4, 3 × 3 × 4 = 36.
Note: You may assume that n is not less than 2 and not larger than 58.
"""


class Solution:
    def integerBreak(self, n: int) -> int:
        if 2 == n:
            return 1
        elif 3 == n:
            return 2
        elif 4 == n:
            return 4
        elif 5 == n:
            return 6
        else:
            if n % 3 == 0:
                return 3**(n//3)
            elif n % 3 == 1:
                return 3**(n//3-1)*4
            else:
                return 3**(n//3)*2


if __name__ == "__main__":
    import timeit
    s = Solution()
    n = 10
    s_t = timeit.default_timer()
    print(s.integerBreak(n))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))