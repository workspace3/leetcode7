# -*- coding: utf-8 -*-
"""
Suppose you have a random list of people standing in a queue.
 Each person is described by a pair of integers (h, k),
 where h is the height of the person and k is the number of people in front of this person
 who have a height greater than or equal to h. Write an algorithm to reconstruct the queue.

Note:
The number of people is less than 1,100.


Example

Input:
[[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]

Output:
[[5,0], [7,0], [5,2], [6,1], [4,4], [7,1]]
"""
from typing import List


class Solution:
    def reconstructQueue(self, people: List[List[int]]) -> List[List[int]]:
        # create index
        group = {}
        for p in people:
            h, n = p
            if h not in group:
                group[h] = []
            group[h].append(p)
        #
        new_queue = []
        length = len(people)
        if length < 2:
            return people
        for key in sorted(group.keys(), reverse=True):
            for curr_people in sorted(group[key], key=lambda x: x[1]):
                pos = curr_people[1]
                new_queue.insert(pos, curr_people)
        return new_queue[:length]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.reconstructQueue(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
