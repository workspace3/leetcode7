# -*- coding: utf-8 -*-
"""
Find the kth largest element in an unsorted array.
Note that it is the kth largest element in the sorted order, not the kth distinct element.

Example 1:

Input: [3,2,1,5,6,4] and k = 2
Output: 5

Example 2:

Input: [3,2,3,1,2,4,5,5,6] and k = 4
Output: 4

Note:
You may assume k is always valid, 1 ≤ k ≤ array's length.

"""
from typing import List


class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        return sorted(nums, reverse=True)[k-1]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [3,2,3,1,2,4,5,5,6]
    k = 4
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.findKthLargest(ss, k))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
