# -*- coding: utf-8 -*-
"""
Given an array nums sorted in ascending order,
return true if and only if you can split it into 1 or more subsequences
such that each subsequence consists of consecutive integers and has length at least 3.



Example 1:

Input: [1,2,3,3,4,5]
Output: True
Explanation:
You can split them into two consecutive subsequences :
1, 2, 3
3, 4, 5

Example 2:

Input: [1,2,3,3,4,4,5,5]
Output: True
Explanation:
You can split them into two consecutive subsequences :
1, 2, 3, 4, 5
3, 4, 5

Example 3:

Input: [1,2,3,4,4,5]
Output: False


Constraints:

1 <= nums.length <= 10000
----------贪婪解法-----------
Intuition

Call a chain a sequence of 3 or more consecutive numbers.

Considering numbers x from left to right, if x can be added to a current chain,
it's at least as good to add x to that chain first, rather than to start a new chain.

Why? If we started with numbers x and greater from the beginning,
the shorter chains starting from x could be concatenated with the chains ending before x,
possibly helping us if there was a "chain" from x that was only length 1 or 2.

Algorithm

Say we have a count of each number, and let tails[x] be the number of chains ending right before x.

Now let's process each number. If there's a chain ending before x, then add it to that chain.
Otherwise, if we can start a new chain, do so.

It's worth noting that our solution can be amended to take only O(1)O(1) additional space,
since we could do our counts similar to Approach #1, and we only need to know the last 3 counts at a time.
---code---
class Solution(object):
    def isPossible(self, nums):
        count = collections.Counter(nums)
        tails = collections.Counter()
        for x in nums:
            if count[x] == 0:
                continue
            elif tails[x] > 0:
                tails[x] -= 1
                tails[x+1] += 1
            elif count[x+1] > 0 and count[x+2] > 0:
                count[x+1] -= 1
                count[x+2] -= 1
                tails[x+3] += 1
            else:
                return False
            count[x] -= 1
        return True
"""
from typing import List
from collections import Counter


class Solution:
    def isPossible(self, nums: List[int]) -> bool:
        return False


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [1,2,3,3,4,4,5,5]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.isPossible(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
