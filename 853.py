# -*- coding: utf-8 -*-
"""
N cars are going to the same destination along a one lane road.
The destination is target miles away.

Each car i has a constant speed speed[i] (in miles per hour),
and initial position position[i] miles towards the target along the road.

A car can never pass another car ahead of it, but it can catch up to it,
and drive bumper to bumper at the same speed.

The distance between these two cars is ignored - they are assumed to have the same position.

A car fleet is some non-empty set of cars driving at the same position and same speed.
Note that a single car is also a car fleet.

If a car catches up to a car fleet right at the destination point,
it will still be considered as one car fleet.


How many car fleets will arrive at the destination?



Example 1:

Input: target = 12, position = [10,8,0,5,3], speed = [2,4,1,1,3]
Output: 3
Explanation:
The cars starting at 10 and 8 become a fleet, meeting each other at 12.
The car starting at 0 doesn't catch up to any other car, so it is a fleet by itself.
The cars starting at 5 and 3 become a fleet, meeting each other at 6.
Note that no other cars meet these fleets before the destination, so the answer is 3.

Note:

0 <= N <= 10 ^ 4
0 < target <= 10 ^ 6
0 < speed[i] <= 10 ^ 6
0 <= position[i] < target
All initial positions are different.
----------其他解法-----------
class Solution(object):
    def carFleet(self, target, position, speed):
        cars = sorted(zip(position, speed))
        times = [float(target - p) / s for p, s in cars]
        ans = 0
        while len(times) > 1:
            lead = times.pop()
            if lead < times[-1]: ans += 1  # if lead arrives sooner, it can't be caught
            else: times[-1] = lead # else, fleet arrives at later time 'lead'

        return ans + bool(times) # remaining car is fleet (if it exists)
"""
from typing import List


class Solution:
    def carFleet(self, target: int, position: List[int], speed: List[int]) -> int:
        def _is_catch_up(a, b):
            if (target-a[0])/a[1] <= (target-b[0])/b[1]:
                return True
            else:
                return False
        #
        if len(position) == 0 or len(position) == 1:
            return len(position)
        has_catch_up = True
        sorted_cars = sorted(zip(position, speed), key=lambda x: x[0])
        next_cars = []
        while has_catch_up:
            has_catch_up = False
            if len(sorted_cars) == 1:
                return 1
            for i in range(1, len(sorted_cars)):
                if not _is_catch_up(sorted_cars[i-1], sorted_cars[i]):
                    next_cars.append(sorted_cars[i-1])
                else:
                    has_catch_up = True
                if i == len(sorted_cars) - 1:
                    next_cars.append(sorted_cars[i])
            sorted_cars = next_cars[0:]
            next_cars = []
        return len(sorted_cars)


if __name__ == "__main__":
    import timeit
    s = Solution()
    target = 12
    position = [10,8,0,5,3]
    speed = [2,4,1,1,3]
    s_t = timeit.default_timer()
    print(s.carFleet(target, position, speed))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
