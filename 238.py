# -*- coding: utf-8 -*-
"""
Given an array nums of n integers where n > 1,
return an array output such that output[i] is
equal to the product of all the elements of nums except nums[i].

Example:

Input:  [1,2,3,4]
Output: [24,12,8,6]
Note: Please solve it without division and in O(n).

Follow up:
Could you solve it with constant space complexity?
(The output array does not count as extra space for the purpose of space complexity analysis.)
"""


class Solution:
    def productExceptSelf(self, nums):
        from functools import reduce
        result = []
        store = {}
        for i in range(len(nums)):
            if nums[i] in store:
                result.append(store[nums[i]])
            else:
                tmp = nums[i]
                nums[i] = 1
                curr = reduce(lambda x, y: x * y, nums)
                result.append(curr)
                nums[i] = tmp
                store[tmp] = curr
        return result

    def productExceptSelf_anather_version(self, nums):
        # reference to code of Jason003
        length = len(nums)
        result = [1] * length
        tmp = 1
        for i in range(0, length - 1):
            tmp *= nums[i]
            result[i + 1] = tmp
        tmp = 1
        for i in range(length - 1, 0, -1):
            tmp *= nums[i]
            result[i - 1] *= tmp
        return result

import timeit
s = Solution()
nums = [1,2,3,4]
print(len(nums))
s_t = timeit.default_timer()
print(s.productExceptSelf_anather_version(nums))
e_t = timeit.default_timer()
print("time cost: {:.5f} seconds.".format(e_t - s_t))