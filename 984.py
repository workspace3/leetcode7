# -*- coding: utf-8 -*-

"""
Given two integers A and B, return any string S such that:

S has length A + B and contains exactly A 'a' letters, and exactly B 'b' letters;
The substring 'aaa' does not occur in S;
The substring 'bbb' does not occur in S.


Example 1:

Input: A = 1, B = 2
Output: "abb"
Explanation: "abb", "bab" and "bba" are all correct answers.
Example 2:

Input: A = 4, B = 1
Output: "aabaa"


Note:

0 <= A <= 100
0 <= B <= 100
It is guaranteed such an S exists for the given A and B.
"""


class Solution:
    def strWithout3a3b(self, A: int, B: int) -> str:
        max_num_a = A // 2
        max_num_b = B // 2
        for i in range(max_num_a+1):
            for j in range(max_num_b+1):
                num_a = A - 2*i + i
                num_b = B - 2*j + j
                if abs(num_a-num_b) < 2:
                    # found
                    strings_a = ["aa"]*i + ["a"]*(A - 2*i)
                    strings_b = ["bb"]*j + ["b"]*(B - 2*j)
                    if num_a < num_b:
                        strings_a.append("")
                        return "".join([b + a for a, b in zip(strings_a, strings_b)])
                    elif num_a > num_b:
                        strings_b.append("")
                        return "".join([a + b for a, b in zip(strings_a, strings_b)])
                    else:
                        return "".join([a + b for a, b in zip(strings_a, strings_b)])
        return ""


if __name__ == "__main__":
    import timeit
    s = Solution()
    A = 1
    B = 1
    s_t = timeit.default_timer()
    print(s.strWithout3a3b(A, B))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
