"""
Given an array A, partition it into two (contiguous) subarrays left and right so that:

Every element in left is less than or equal to every element in right.
left and right are non-empty.
left has the smallest possible size.
Return the length of left after such a partitioning.  It is guaranteed that such a partitioning exists.



Example 1:

Input: [5,0,3,8,6]
Output: 3
Explanation: left = [5,0,3], right = [8,6]
Example 2:

Input: [1,1,1,0,6,12]
Output: 4
Explanation: left = [1,1,1,0], right = [6,12]


Note:

2 <= A.length <= 30000
0 <= A[i] <= 10^6
It is guaranteed there is at least one way to partition A as described.

------------------------------------
其他方案：
Approach 1: Next Array
Intuition

Instead of checking whether all(L <= R for L in left for R in right),
let's check whether max(left) <= min(right).

Algorithm

Let's try to find max(left) for subarrays left = A[:1], left = A[:2], left = A[:3], ... etc.
Specifically, maxleft[i] will be the maximum of subarray A[:i].
They are related to each other: max(A[:4]) = max(max(A[:3]), A[3]), so maxleft[4] = max(maxleft[3], A[3]).

Similarly, min(right) for every possible right can be found in linear time.

After we have a way to query max(left) and min(right) quickly, the solution is straightforward.
"""


class Solution:
    def partitionDisjoint(self, A) -> int:
        left = [A[0]]
        right = A[1:]
        span_left = [A[0], A[0]]
        span_right = [min(right), max(right)]
        while span_left[1] > span_right[0]:
            curr_num = right.pop(0)
            if curr_num > span_left[1]:
                span_left[1] = curr_num
            if curr_num < span_left[0]:
                span_left[0] = curr_num
            if curr_num == span_right[0]:
                span_right[0] = min(right)
            if curr_num == span_right[1]:
                span_right[1] = max(right)
            left.append(curr_num)
        return len(left)


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [1,1,1,0,6,12]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.partitionDisjoint(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))