# -*- coding: utf-8 -*-
"""
Given an array nums of n integers and an integer target,
are there elements a, b, c, and d in nums such that a + b + c + d = target?
Find all unique quadruplets in the array which gives the sum of target.

Note:

The solution set must not contain duplicate quadruplets.

Example:

Given array nums = [1, 0, -1, 0, -2, 2], and target = 0.

A solution set is:
[
  [-1,  0, 0, 1],
  [-2, -1, 1, 2],
  [-2,  0, 0, 2]
]
"""
from typing import List


class Solution:
    def fourSum(self, nums: List[int], target: int) -> List[List[int]]:
        # count all two sums
        length = len(nums)
        two_sums = {}
        for i in range(length):
            for j in range(i+1, length):
                curr_sum = nums[i] + nums[j]
                if curr_sum not in two_sums:
                    two_sums[curr_sum] = []
                two_sums[curr_sum].append([i, j])
        result = set()
        for key in two_sums:
            another_key = target - key
            if another_key in two_sums:
                for left in two_sums[key]:
                    for right in two_sums[another_key]:
                        if set(left) & set(right):
                            continue
                        comb = str(sorted([nums[k] for k in left + right]))
                        result.add(comb)
        result = [eval(x) for x in list(result)]
        return result


if __name__ == "__main__":
    import timeit
    s = Solution()
    nums = [1, 0, -1, 0, -2, 2]
    target = 0
    s_t = timeit.default_timer()
    print(s.fourSum(nums, target))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))

