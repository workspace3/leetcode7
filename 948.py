"""
You have an initial power P, an initial score of 0 points, and a bag of tokens.

Each token can be used at most once, has a value token[i], and has potentially two ways to use it.

If we have at least token[i] power, we may play the token face up, losing token[i] power, and gaining 1 point.
If we have at least 1 point, we may play the token face down, gaining token[i] power, and losing 1 point.
Return the largest number of points we can have after playing any number of tokens.



Example 1:

Input: tokens = [100], P = 50
Output: 0
Example 2:

Input: tokens = [100,200], P = 150
Output: 1
Example 3:

Input: tokens = [100,200,300,400], P = 200
Output: 2


Note:

tokens.length <= 1000
0 <= tokens[i] < 10000
0 <= P < 10000
"""
from typing import List


class Solution:
    def bagOfTokensScore(self, tokens: List[int], P: int) -> int:
        if len(tokens) == 0:
            return 0
        tokens = sorted(tokens)
        max_points = 0
        points = 0
        i, j = 0, len(tokens)-1
        while True:
            if i < len(tokens) and tokens[i] <= P:
                points += 1
                P -= tokens[i]
                i += 1
                if points > max_points:
                    max_points = points
            else:
                if i < len(tokens) - 2 and points > 0:
                    P += tokens[j]
                    j -= 1
                    points -= 1
                else:
                    break
        return max_points


if __name__ == "__main__":
    import timeit
    s = Solution()
    tokens = [4903,8812,6101,4671,6323,3378,1243,6825,6220,7885,1271,9117,7993,9168,8725]
    P = 6810
    s_t = timeit.default_timer()
    print(s.bagOfTokensScore(tokens, P))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))