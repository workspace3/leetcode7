"""
Find the smallest prime palindrome greater than or equal to N.

Recall that a number is prime if it's only divisors are 1 and itself, and it is greater than 1.

For example, 2,3,5,7,11 and 13 are primes.

Recall that a number is a palindrome if it reads the same from left to right as it does from right to left.

For example, 12321 is a palindrome.



Example 1:

Input: 6
Output: 7
Example 2:

Input: 8
Output: 11
Example 3:

Input: 13
Output: 101


Note:

1 <= N <= 10^8
The answer is guaranteed to exist and be less than 2 * 10^8.
"""


class Solution:
    def primePalindrome(self, N: int) -> int:
        def is_prime(n):
            if 1 == n:
                return False
            elif 2 == n:
                return True
            else:
                for i in range(2, n):
                    if i**2 > n:
                        break
                    if n % i == 0:
                        return False
                return True
        #
        def is_palindrome(n):
            if str(n) == str(n)[-1::-1]:
                return True
            else:
                return False
        #
        n = N
        while n < 2 * (10**8):
            if is_prime(n) and is_palindrome(n):
                return n
            n += 1


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = 10000000
    s_t = timeit.default_timer()
    print(s.primePalindrome(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))