"""
Given an integer array, you need to find one continuous subarray that
 if you only sort this subarray in ascending order,
 then the whole array will be sorted in ascending order, too.

You need to find the shortest such subarray and output its length.

Example 1:
Input: [2, 6, 4, 8, 10, 9, 15]
Output: 5
Explanation: You need to sort [6, 4, 8, 10, 9] in ascending order
to make the whole array sorted in ascending order.
Note:
Then length of the input array is in range [1, 10,000].
The input array may contain duplicates, so ascending order here means <=.
-----------------
参考网站：
https://leetcode.com/problems/shortest-unsorted-continuous-subarray/solution/
"""
from typing import List


class Solution:
    def findUnsortedSubarray(self, nums: List[int]) -> int:
        sorted_nums = sorted(nums)
        result = [True if x == y else False for x, y in zip(nums, sorted_nums)]
        if not all(result):
            return len(nums) - result.index(False) - result[-1::-1].index(False)
        else:
            return 0


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [2,3,3,2,4]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.findUnsortedSubarray(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
