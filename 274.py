# -*- coding: utf-8 -*-
"""
Given an array of citations (each citation is a non-negative integer) of a researcher,
write a function to compute the researcher's h-index.

According to the definition of h-index on Wikipedia:
"A scientist has index h if h of his/her N papers have at least h citations each,
and the other N − h papers have no more than h citations each."

Example:

Input: citations = [3,0,6,1,5]
Output: 3
Explanation: [3,0,6,1,5] means the researcher has 5 papers in total and each of them had
             received 3, 0, 6, 1, 5 citations respectively.
             Since the researcher has 3 papers with at least 3 citations each and the remaining
             two with no more than 3 citations each, her h-index is 3.
Note: If there are several possible values for h, the maximum one is taken as the h-index.


"""
from typing import List


class Solution:
    def hIndex(self, citations: List[int]) -> int:
        citations = sorted(citations, reverse=True)
        for i in range(1, len(citations)+1)[::-1]:
            c = i
            curr_v = citations[i-1]
            next_v = citations[i+1] if i+1 < len(citations) else -float("inf")
            if next_v <= c <= curr_v:
                return c
        return 0


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [2,0,4,1,5]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.hIndex(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))

