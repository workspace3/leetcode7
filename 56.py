"""
Given a collection of intervals, merge all overlapping intervals.

Example 1:

Input: [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
Example 2:

Input: [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considered overlapping.
NOTE: input types have been changed on April 15, 2019.
Please reset to default code definition to get new method signature.
"""


class Solution:
    def merge(self, intervals):
        def is_overlap(left, right):
            if left[1] < right[0]:
                return False
            else:
                return True
        #
        if len(intervals) == 1:
            return intervals
        intervals = sorted(intervals, key=lambda x: x[0])
        i = 0
        length = len(intervals)
        while i < length:
            for j in range(i+1, length):
                if is_overlap(intervals[i], intervals[j]):
                    intervals[i] = [intervals[i][0], max(intervals[j][1], intervals[i][1])]
                    if j == length - 1:
                        intervals = intervals[:i] + [intervals[i]]
                else:
                    intervals = intervals[:i] + [intervals[i]] + intervals[j:]
                    break
            i += 1
            length = len(intervals)
        return intervals


if __name__ == "__main__":
    import timeit
    s = Solution()
    input_data = [[1,4],[4,5]]
    print(len(input_data))
    s_t = timeit.default_timer()
    print(s.merge(input_data))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
