# -*- coding: utf-8 -*-
"""
In an array A of 0s and 1s, how many non-empty subarrays have sum S?



Example 1:

Input: A = [1,0,1,0,1], S = 2
Output: 4
Explanation:
The 4 subarrays are bolded below:
[1,0,1,0,1]
[1,0,1,0,1]
[1,0,1,0,1]
[1,0,1,0,1]


Note:

A.length <= 30000
0 <= S <= A.length
A[i] is either 0 or 1.
"""
from typing import List


class Solution:
    def numSubarraysWithSum(self, A: List[int], S: int) -> int:
        length = len(A)
        ones = []
        count_zeros_left = count_zeros_right = result = 0
        for i in range(length):
            if 0 == A[i]:
                if len(ones) == 0:
                    count_zeros_left += 1
                elif len(ones) == S:
                    count_zeros_right += 1
            else:
                ones.append(i)
                if len(ones) == S + 1:
                    result += (count_zeros_left+1) * (count_zeros_right+1)
                    count_zeros_left = ones[1] - ones[0] - 1
                    count_zeros_right = 0
                    _ = ones.pop(0)
            if length - 1 == i:
                if len(ones) == S:
                    result += (count_zeros_left+1) * (count_zeros_right+1)
        return result


if __name__ == "__main__":
    import timeit
    s = Solution()
    A = [0,0,0,0,0]
    S = 0
    s_t = timeit.default_timer()
    print(s.numSubarraysWithSum(A, S))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
