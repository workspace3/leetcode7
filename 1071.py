"""
For strings S and T, we say "T divides S" if and only if S = T + ... + T
(T concatenated with itself 1 or more times)

Return the largest string X such that X divides str1 and X divides str2.



Example 1:

Input: str1 = "ABCABC", str2 = "ABC"
Output: "ABC"
Example 2:

Input: str1 = "ABABAB", str2 = "ABAB"
Output: "AB"
Example 3:

Input: str1 = "LEET", str2 = "CODE"
Output: ""


Note:

1 <= str1.length <= 1000
1 <= str2.length <= 1000
str1[i] and str2[i] are English uppercase letters.
"""


class Solution:
    def gcdOfStrings(self, str1: str, str2: str) -> str:
        if len(str1) > len(str2):
            tmp = str1[:]
            str1 = str2[:]
            str2 = tmp[:]
        length_1 = len(str1)
        length_2 = len(str2)
        for i in range(1, length_1+1):
            if length_1 % i == 0:
                sub_len = length_1 // i
                tmp = set([str1[x:x+sub_len] for x in range(0, length_1, sub_len)])
                tmp_str2 = str1[:sub_len]*(length_2//sub_len)
                if len(tmp) == 1 and length_2 % sub_len == 0 and str2 == tmp_str2:
                    return str1[:sub_len]
        return ""


if __name__ == "__main__":
    import timeit
    s = Solution()
    str1 = "LEET"
    str2 = "CODE"
    s_t = timeit.default_timer()
    print(s.gcdOfStrings(str1, str2))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
