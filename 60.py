# -*- coding: utf-8 -*-
"""
The set [1,2,3,...,n] contains a total of n! unique permutations.

By listing and labeling all of the permutations in order,
we get the following sequence for n = 3:

"123"
"132"
"213"
"231"
"312"
"321"
Given n and k, return the k-th permutation sequence.
"""


class Solution:
    def getPermutation(self, n: int, k: int) -> str:
        def nextPermutation(nums):
            def _find_first_down(numbers):
                l = len(numbers)
                for i in range(l - 1, -1, -1):
                    if i + 1 < l and numbers[i] < numbers[i + 1]:
                        return i
                return -1

            #
            def _find_last_bigger(curr_pos, numbers):
                l = len(numbers)
                i = curr_pos + 1
                while i < l:
                    if numbers[i] > numbers[curr_pos]:
                        pass
                    else:
                        return i - 1
                    i += 1
                if i == l:
                    return l - 1
                else:
                    return -1

            #
            def _swap(i, j):
                tmp = nums[i]
                nums[i] = nums[j]
                nums[j] = tmp

            #
            length = len(nums)
            if 0 == length:
                pass
            else:
                first_down_pos = _find_first_down(nums)
                if -1 == first_down_pos:
                    nums[0:] = nums[-1::-1]
                else:
                    first_bigger_pos = _find_last_bigger(first_down_pos, nums)
                    if -1 == first_bigger_pos:
                        pass
                    else:
                        _swap(first_down_pos, first_bigger_pos)
                        nums[first_down_pos + 1:] = nums[-1:first_down_pos:-1]
        #
        nums = [i for i in range(1, n+1)]
        count = 1
        while count < k:
            nextPermutation(nums)
            count += 1
        return "".join([str(j) for j in nums])


if __name__ == "__main__":
    import timeit
    s = Solution()
    n = 9
    k = 135401
    s_t = timeit.default_timer()
    print(s.getPermutation(n, k))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
