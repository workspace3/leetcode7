"""
Additive number is a string whose digits can form additive sequence.

A valid additive sequence should contain at least three numbers.
Except for the first two numbers, each subsequent number in the sequence must be the sum of the preceding two.

Given a string containing only digits '0'-'9', write a function to determine if it's an additive number.

Note: Numbers in the additive sequence cannot have leading zeros, so sequence 1, 2, 03 or 1, 02, 3 is invalid.

Example 1:

Input: "112358"
Output: true
Explanation: The digits can form an additive sequence: 1, 1, 2, 3, 5, 8.
             1 + 1 = 2, 1 + 2 = 3, 2 + 3 = 5, 3 + 5 = 8
Example 2:

Input: "199100199"
Output: true
Explanation: The additive sequence is: 1, 99, 100, 199.
             1 + 99 = 100, 99 + 100 = 199
Follow up:
How would you handle overflow for very large input integers?
"""


class Solution:
    def isAdditiveNumber(self, num: str) -> bool:
        length = len(num)
        if length < 3:
            return False
        elif 3 == length:
            if int(num[0]) + int(num[1]) == int(num[2]):
                return True
            else:
                return False
        for i in range(1, min(length//2+2, length)):
            if '0' == num[0] and i > 1:
                break
            curr_nums = [int(num[:i])]
            j = i + 1
            while j <length:
                k = j
                curr_nums.append(int(num[i:k]))
                while k < length:
                    if '0' == num[i] and j > i + 1:
                        break
                    next_num = curr_nums[-2] + curr_nums[-1]
                    if num[k:].startswith(str(next_num)):
                        curr_nums.append(next_num)
                        k += len(str(next_num))
                    else:
                        break
                if length == k and len(curr_nums) > 2:
                    return True
                j += 1
                curr_nums = [int(num[:i])]
        return False


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "211738"
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.isAdditiveNumber(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))