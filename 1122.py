# -*- coding: utf-8 -*-
"""
Given two arrays arr1 and arr2, the elements of arr2 are distinct,
and all elements in arr2 are also in arr1.

Sort the elements of arr1 such that the relative ordering of items in arr1 are the same as in arr2.
Elements that don't appear in arr2 should be placed at the end of arr1 in ascending order.



Example 1:

Input: arr1 = [2,3,1,3,2,4,6,7,9,2,19], arr2 = [2,1,4,3,9,6]
Output: [2,2,2,1,4,3,3,9,6,7,19]


Constraints:

arr1.length, arr2.length <= 1000
0 <= arr1[i], arr2[i] <= 1000
Each arr2[i] is distinct.
Each arr2[i] is in arr1.
"""
from typing import List


class Solution:
    def relativeSortArray(self, arr1: List[int], arr2: List[int]) -> List[int]:
        #
        value2index_of_arr2 = dict([(n, i) for i, n in enumerate(arr2)])

        def _is_less_than(aa, bb):
            if aa in value2index_of_arr2 and bb in value2index_of_arr2:
                return value2index_of_arr2[aa] < value2index_of_arr2[bb]
            elif aa in value2index_of_arr2 and bb not in value2index_of_arr2:
                return True
            elif aa not in value2index_of_arr2 and bb in value2index_of_arr2:
                return False
            else:
                return aa < bb

        def _merge(a, b):
            c = []
            i = j = 0
            while i < len(a) and j < len(b):
                if _is_less_than(a[i], b[j]):
                    c.append(a[i])
                    i += 1
                else:
                    c.append(b[j])
                    j += 1
            if i == len(a):
                c.extend(b[j:])
            elif j == len(b):
                c.extend(a[i:])
            else:
                pass
            return c

        def merge_sort(a):
            if len(a) == 1:
                return a
            else:
                mid_index = len(a) // 2
                left, right = merge_sort(a[0:mid_index]), merge_sort(a[mid_index:])
                out = _merge(left, right)
                return out

        #
        result = merge_sort(arr1)
        return result


if __name__ == "__main__":
    import timeit
    s = Solution()
    arr1 = [2,3,1,3,2,4,6,7,9,2,19]
    arr2 = [2,1,4,3,9,6]
    s_t = timeit.default_timer()
    print(s.relativeSortArray(arr1, arr2))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))

