"""
Given a string S and a character C,
return an array of integers representing the shortest distance from the character C in the string.

Example 1:

Input: S = "loveleetcode", C = 'e'
Output: [3, 2, 1, 0, 1, 0, 0, 1, 2, 2, 1, 0]


Note:

S string length is in [1, 10000].
C is a single character, and guaranteed to be in string S.
All letters in S and C are lowercase.
"""


class Solution:
    def shortestToChar(self, S, C):
        parts = S.split(C)
        result = []
        for i in range(len(parts)):
            curr_part = parts[i]
            if not curr_part:
                result.append(0)
                continue
            if i - 1 < 0:
                left = [float("inf")] * len(curr_part)
            else:
                left = [1+x for x in range(len(curr_part))]
            if i + 1 >= len(parts):
                right = [float("inf")] * len(curr_part)
            else:
                right = [1+x for x in range(len(curr_part))][-1::-1]
            final = [min(x, y) for x, y in zip(left, right)]
            result.extend(final)
            result.append(0)
        _ = result.pop()
        return result


import timeit
s = Solution()
S = "loveleetcode"
C = 'e'
s_t = timeit.default_timer()
print(s.shortestToChar(S, C))
e_t = timeit.default_timer()
print("time cost: {:.5f} seconds.".format(e_t - s_t))
