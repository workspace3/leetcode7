"""
The i-th person has weight people[i], and each boat can carry a maximum weight of limit.

Each boat carries at most 2 people at the same time,
provided the sum of the weight of those people is at most limit.

Return the minimum number of boats to carry every given person.
(It is guaranteed each person can be carried by a boat.)



Example 1:

Input: people = [1,2], limit = 3
Output: 1
Explanation: 1 boat (1, 2)
Example 2:

Input: people = [3,2,2,1], limit = 3
Output: 3
Explanation: 3 boats (1, 2), (2) and (3)
Example 3:

Input: people = [3,5,3,4], limit = 5
Output: 4
Explanation: 4 boats (3), (3), (4), (5)
Note:

1 <= people.length <= 50000
1 <= people[i] <= limit <= 30000
"""


class Solution:
    def numRescueBoats(self, people, limit: int) -> int:
        from collections import Counter
        boats = 0
        count = Counter(people)
        people = sorted(list(count.keys()))
        while people:
            if len(people) == 1:
                if people[0]*2 <= limit:
                    boats += count[people[0]] // 2
                    if count[people[0]] % 2 == 1:
                        boats += 1
                else:
                    boats += count[people[0]]
                break
            if people[0] + people[-1] <= limit:
                diff = min(count[people[0]], count[people[-1]])
                boats += diff
                count[people[0]] -= diff
                count[people[-1]] -= diff
                if 0 == count[people[0]]:
                    people = people[1:]
                if 0 == count[people[-1]]:
                    people = people[:-1]
            else:
                boats += count[people[-1]]
                people = people[:-1]
        return boats


if __name__ == "__main__":
    import timeit
    s = Solution()
    people = [3,5,3,4]
    limit = 5
    s_t = timeit.default_timer()
    print(s.numRescueBoats(people, limit))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
