# -*- coding: utf-8 -*-
"""
Given a list of non negative integers, arrange them such that they form the largest number.

Example 1:

Input: [10,2]
Output: "210"
Example 2:

Input: [3,30,34,5,9]
Output: "9534330"
Note: The result may be very large, so you need to return a string instead of an integer.
"""
from typing import List
import functools


class Solution:
    def largestNumber(self, nums: List[int]) -> str:
        # self-defined comparing
        def Cmp(x, y):
            return int(x + y) - int(y + x)
        #
        nums = sorted(map(str, nums), key=functools.cmp_to_key(Cmp), reverse=True)
        largest_num = "".join(nums)
        return largest_num


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [3,30,34,5,9]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.largestNumber(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))

