# -*- coding: utf-8 -*-
"""
Given an 2D board, count how many battleships are in it.
The battleships are represented with 'X's, empty slots are represented with '.'s.
You may assume the following rules:
You receive a valid board, made of only battleships or empty slots.
Battleships can only be placed horizontally or vertically.
In other words, they can only be made of the shape 1xN (1 row, N columns) or Nx1 (N rows, 1 column),
where N can be of any size.
At least one horizontal or vertical cell separates between two battleships - there are no adjacent battleships.
Example:
X..X
...X
...X
In the above board there are 2 battleships.
Invalid Example:
...X
XXXX
...X
This is an invalid board that you will not receive - as battleships will always have a cell separating between them.
Follow up:
Could you do it in one-pass, using only O(1) extra memory and without modifying the value of the board?
"""
from typing import List


class Solution:
    def countBattleships(self, board: List[List[str]]) -> int:
        # dfs
        row_len, col_len = len(board), len(board[0])
        tag = [[0 for _ in range(col_len)] for _ in range(row_len)]

        def dfs(x, y):
            """
            从(x, y)出发，搜索一条可能的ship，并标记
            :param x:
            :param y:
            :return:
            """
            if "." == board[x][y] or \
                1 == tag[x][y] or \
                    x-1 >= 0 and "X" == board[x-1][y] or \
                    y-1 >= 0 and "X" == board[x][y-1]:
                return 0
            else:
                # 向右搜索
                while True:
                    new_y = y + 1
                    if new_y >= col_len or "." == board[x][new_y]:
                        return 1
                    elif 1 == tag[x][new_y]:
                        break
                    else:
                        tag[x][new_y] = 1
                        if x-1 >= 0 and "X" == board[x-1][new_y] or \
                                x+1 < row_len and "X" == board[x+1][new_y]:
                            break
                # 向下搜索
                while True:
                    new_x = x + 1
                    if new_x >= row_len or "." == board[new_x][y]:
                        return 1
                    elif 1 == tag[new_x][y]:
                        break
                    else:
                        tag[new_x][y] = 1
                        if y-1 >= 0 and "X" == board[new_x][y-1] or \
                                y+1 < col_len and "X" == board[new_x][y+1]:
                            break
                #
                return 0

        #
        count = 0
        for r in range(row_len):
            for c in range(col_len):
                if "." == board[r][c]:
                    continue
                if not(r-1 >= 0 and "X" == board[r-1][c] or c-1 >= 0 and "X" == board[r][c-1]):
                    count += 1
                else:
                    pass
        return count


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [["X",".",".","X"],[".",".",".","X"],[".",".",".","X"]]
    # ss = [["X","X","X"]]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.countBattleships(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
