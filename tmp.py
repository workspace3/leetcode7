# -*- coding: utf-8 -*-
"""
"""
import timeit


def _merge(a, b):
    c = []
    i = j = 0
    while i < len(a) and j < len(b):
        if a[i] < b[j]:
            c.append(a[i])
            i += 1
        else:
            c.append(b[j])
            j += 1
    if i == len(a):
        c.extend(b[j:])
    elif j == len(b):
        c.extend(a[i:])
    else:
        pass
    return c


def merge_sort(a, memo):
    # if str(a) in memo:
    #     return memo.get(str(a))
    if len(a) == 1:
        # memo[str(a)] = a
        return a
    else:
        mid_index = len(a) // 2
        left, right = merge_sort(a[0:mid_index], memo), merge_sort(a[mid_index:], memo)
        out = _merge(left, right)
        # memo[str(a)] = out
        return out


#
import random
random.seed(0)
t = [x for x in range(10000)]
random.shuffle(t)
memo = dict()
s = timeit.default_timer()
result = merge_sort(t, memo)
e = timeit.default_timer()
print("input: {}".format(t))
print("result: {}".format(result))
print("time cost: {0:.10f} seconds.".format(e - s))

import numpy as np
import random
a = [x for x in range(5000)]
random.shuffle(a)
a = np.array(a)
s = timeit.default_timer()
b = a.tolist()
e = timeit.default_timer()
print("1)time cost: {0:.10f} seconds.".format(e - s))
s = timeit.default_timer()
c = list(a)
e = timeit.default_timer()
print("2)time cost: {0:.10f} seconds.".format(e - s))