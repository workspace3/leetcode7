# -*- coding: utf-8 -*-
"""
Given a string s, partition s such that every substring of the partition is a palindrome.

Return all possible palindrome partitioning of s.

Example:

Input: "aab"
Output:
[
  ["aa","b"],
  ["a","a","b"]
]

"""
from typing import List


class Solution:
    def partition(self, s: str) -> List[List[str]]:
        return [[""]]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "asdsdsdsd"
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.partition(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))

