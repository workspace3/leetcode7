"""
Given an array of unique integers, each integer is strictly greater than 1.

We make a binary tree using these integers and each number may be used for any number of times.

Each non-leaf node's value should be equal to the product of the values of it's children.

How many binary trees can we make?  Return the answer modulo 10 ** 9 + 7.

Example 1:

Input: A = [2, 4]
Output: 3
Explanation: We can make these trees: [2], [4], [4, 2, 2]
Example 2:

Input: A = [2, 4, 5, 10]
Output: 7
Explanation: We can make these trees: [2], [4], [5], [10], [4, 2, 2], [10, 2, 5], [10, 5, 2].


Note:

1 <= A.length <= 1000.
2 <= A[i] <= 10 ^ 9.
"""


class Solution:
    def numFactoredBinaryTrees(self, A) -> int:
        MOD = 10**9+7
        A_for_search = set(A)
        A = sorted(A)
        length = len(A)
        dp = [1]*length
        for i in range(length):
            for j in range(i):
                if A[i] % A[j] == 0:
                    target = A[i]//A[j]
                    if target == A[j]:
                        dp[i] += dp[j]**2
                    elif target in A_for_search:
                        dp[i] += (dp[j] * dp[A.index(target)])
        return sum(dp) % MOD


if __name__ == "__main__":
    import timeit
    s = Solution()
    A = [46,144,5040,4488,544,380,4410,34,11,5,3063808,5550,34496,12,540,28,18,13,2,1056,32710656,31,91872,23,26,240,18720,33,49,4,38,37,1457,3,799,557568,32,1400,47,10,20774,1296,9,21,92928,8704,29,2162,22,1883700,49588,1078,36,44,352,546,19,523370496,476,24,6000,42,30,8,16262400,61600,41,24150,1968,7056,7,35,16,87,20,2730,11616,10912,690,150,25,6,14,1689120,43,3128,27,197472,45,15,585,21645,39,40,2205,17,48,136]
    print(len(A))
    s_t = timeit.default_timer()
    print(s.numFactoredBinaryTrees(A))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))