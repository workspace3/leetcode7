"""
Implement a MyCalendarTwo class to store your events.
A new event can be added if adding the event will not cause a triple booking.

Your class will have one method, book(int start, int end). Formally,
this represents a booking on the half open interval [start, end),
the range of real numbers x such that start <= x < end.

A triple booking happens when three events have some non-empty intersection
(ie., there is some time that is common to all 3 events.)

For each call to the method MyCalendar.book,
return true if the event can be added to the calendar successfully
without causing a triple booking. Otherwise,
return false and do not add the event to the calendar.

Your class will be called like this:
MyCalendar cal = new MyCalendar();
MyCalendar.book(start, end)

Example 1:

MyCalendar();
MyCalendar.book(10, 20); // returns true
MyCalendar.book(50, 60); // returns true
MyCalendar.book(10, 40); // returns true
MyCalendar.book(5, 15); // returns false
MyCalendar.book(5, 10); // returns true
MyCalendar.book(25, 55); // returns true
Explanation:
The first two events can be booked.  The third event can be double booked.
The fourth event (5, 15) can't be booked, because it would result in a triple booking.
The fifth event (5, 10) can be booked, as it does not use time 10 which is already double booked.
The sixth event (25, 55) can be booked, as the time in [25, 40) will be double booked with the third event;
the time [40, 50) will be single booked, and the time [50, 55) will be double booked with the second event.


Note:

The number of calls to MyCalendar.book per test case will be at most 1000.
In calls to MyCalendar.book(start, end), start and end are integers in the range [0, 10^9].
"""


class MyCalendar:

    def __init__(self):
        self.records = []

    def book(self, start: int, end: int) -> bool:
        def _compare(record_a, record_b):
            if record_a[1] <= record_b[0]:
                return -1
            elif record_a[0] >= record_b[1]:
                return 1
            else:
                return 0
        #
        length = len(self.records)
        count = 0
        sorted_by_start = sorted(self.records, key=lambda x: x[0])
        sorted_by_end = sorted(self.records, key=lambda x: x[1])
        new_record = (start, end)
        for i in range(length):
            curr_record_by_start = sorted_by_start[i]
            curr_record_by_end = sorted_by_end[i]
            if 0 == _compare(new_record, curr_record_by_start):
                count += 1
            if curr_record_by_end != curr_record_by_start and 0 == _compare(new_record, curr_record_by_end):
                count += 1
            if count > 1:
                return False
        self.records.append(new_record)
        return True


if __name__ == "__main__":
    import timeit
    obj = MyCalendar()
    s_t = timeit.default_timer()
    for start, end in [[10,20],[50,60],[10,40],[5,15],[5,10],[25,55]]:
        if 25 == start and 55 == end:
            a = 1
        print((start, end), obj.book(start, end))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
