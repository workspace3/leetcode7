# -*- coding: utf-8 -*-
"""
A message containing letters from A-Z is being encoded to numbers using the following mapping:

'A' -> 1
'B' -> 2
...
'Z' -> 26
Given a non-empty string containing only digits, determine the total number of ways to decode it.

Example 1:

Input: "12"
Output: 2
Explanation: It could be decoded as "AB" (1 2) or "L" (12).
Example 2:

Input: "226"
Output: 3
Explanation: It could be decoded as "BZ" (2 26), "VF" (22 6), or "BBF" (2 2 6).
"""


class Solution:
    def numDecodings(self, s: str) -> int:
        length = len(s)
        if '0' == s[0]:
            return 0
        if 1 == length:
            return 1
        dp = [1]*(length+1)
        for i in range(2, length+1):
            two_digits = int(s[i-2:i])
            one_digit = int(s[i-1])
            if 0 == one_digit:
                if two_digits in range(10, 27):
                    dp[i] = dp[i-2]
                else:
                    return 0
            else:
                if two_digits in range(10, 27):
                    dp[i] = dp[i-1] + dp[i-2]
                else:
                    dp[i] = dp[i-1]
        return dp[-1]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "206"
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.numDecodings(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
