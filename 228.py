# -*- coding: utf-8 -*-
"""
Given a sorted integer array without duplicates, return the summary of its ranges.

Example 1:

Input:  [0,1,2,4,5,7]
Output: ["0->2","4->5","7"]
Explanation: 0,1,2 form a continuous range; 4,5 form a continuous range.
Example 2:

Input:  [0,2,3,4,6,8,9]
Output: ["0","2->4","6","8->9"]
Explanation: 2,3,4 form a continuous range; 8,9 form a continuous range.
"""
from typing import List


class Solution:
    def summaryRanges(self, nums: List[int]) -> List[str]:
        if len(nums) == 0:
            return []
        result = [[]]
        for n in nums:
            if len(result[-1]) == 0:
                result[-1].append(n)
            else:
                if n - 1 == result[-1][-1]:
                    result[-1].append(n)
                else:
                    result.append([n])
        rt = [str(item[0]) if len(item) == 1 else "{0:d}->{1:d}".format(item[0], item[-1])for item in result]
        return rt


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [0,1,2,4,5,7]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.summaryRanges(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
