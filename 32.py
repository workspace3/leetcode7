# -*- coding: utf-8 -*-
"""
Write a program to solve a Sudoku puzzle by filling the empty cells.

A sudoku solution must satisfy all of the following rules:

Each of the digits 1-9 must occur exactly once in each row.
Each of the digits 1-9 must occur exactly once in each column.
Each of the the digits 1-9 must occur exactly once in each of the 9 3x3 sub-boxes of the grid.
Empty cells are indicated by the character '.'.
"""
from typing import List


class Solution:
    def solveSudoku(self, board: List[List[str]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """
        blank = [(i, j) for i in range(9) for j in range(9) if "." == board[i][j]]
        blank_count = len(blank)
        digits = [str(i) for i in range(1, 10)]

        #
        def is_valid(x, y, val):
            # row check
            flags = list()
            flags.append(val not in board[x])
            flags.append(val not in [board[i][y] for i in range(9)])
            if 0 <= x < 3:
                i1, i2 = 0, 3
            elif 3 <= x < 6:
                i1, i2 = 3, 6
            else:
                i1, i2 = 6, 9
            if 0 <= y < 3:
                j1, j2 = 0, 3
            elif 3 <= y < 6:
                j1, j2 = 3, 6
            else:
                j1, j2 = 6, 9
            flags.append(val not in [board[i][j] for i in range(i1, i2) for j in range(j1, j2)])
            if all(flags):
                return True
            else:
                return False

        #
        def helper(num):
            if num == blank_count:
                return True
            x, y = blank[num]
            for d in digits:
                if is_valid(x, y, d):
                    board[x][y] = d
                    if helper(num + 1):
                        return True
                    board[x][y] = "."
            return False
        #
        helper(0)


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [["5","3",".",".","7",".",".",".","."],["6",".",".","1","9","5",".",".","."],[".","9","8",".",".",".",".","6","."],["8",".",".",".","6",".",".",".","3"],["4",".",".","8",".","3",".",".","1"],["7",".",".",".","2",".",".",".","6"],[".","6",".",".",".",".","2","8","."],[".",".",".","4","1","9",".",".","5"],[".",".",".",".","8",".",".","7","9"]]
    print(len(ss))
    s_t = timeit.default_timer()
    s.solveSudoku(ss)
    print("\n".join(map(str, ss)))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
