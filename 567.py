"""
Given two strings s1 and s2, write a function to return true if s2 contains the permutation of s1.
In other words, one of the first string's permutations is the substring of the second string.



Example 1:

Input: s1 = "ab" s2 = "eidbaooo"
Output: True
Explanation: s2 contains one permutation of s1 ("ba").
Example 2:

Input:s1= "ab" s2 = "eidboaoo"
Output: False


Note:

The input strings only contain lower case letters.
The length of both given strings is in range [1, 10,000].
"""


class Solution:
    def checkInclusion(self, s1: str, s2: str) -> bool:
        s2_len = len(s2)
        s1_len = len(s1)
        base_sub_string = s2[:s1_len]
        s1_count = [0]*26
        s2_count = [0]*26
        for c in s1:
            s1_count[ord(c)-ord('a')] += 1
        for c in base_sub_string:
            s2_count[ord(c)-ord('a')] += 1
        is_same = [s1_count[j] == s2_count[j] for j in range(26)]
        if all(is_same):
            return True
        for i in range(s1_len, s2_len):
            del_c = base_sub_string[0]
            add_c = s2[i]
            base_sub_string = base_sub_string[1:] + add_c
            del_pos = ord(del_c)-ord('a')
            add_pos = ord(add_c)-ord('a')
            s2_count[del_pos] -= 1
            s2_count[add_pos] += 1
            if s2_count[del_pos] == s1_count[del_pos]:
                is_same[del_pos] = True
            else:
                is_same[del_pos] = False
            if s2_count[add_pos] == s1_count[add_pos]:
                is_same[add_pos] = True
            else:
                is_same[add_pos] = False
            if all(is_same):
                return True
        return False


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "dd"
    sss = "rroogzkdktk"
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.checkInclusion(ss, sss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
