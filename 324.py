# -*- coding: utf-8 -*-
"""
Given an unsorted array nums, reorder it such that nums[0] < nums[1] > nums[2] < nums[3]....

Example 1:

Input: nums = [1, 5, 1, 1, 6, 4]
Output: One possible answer is [1, 4, 1, 5, 1, 6].

Example 2:

Input: nums = [1, 3, 2, 2, 3, 1]
Output: One possible answer is [2, 3, 1, 3, 1, 2].

Note:
You may assume all input has valid answer.

Follow Up:
Can you do it in O(n) time and/or in-place with O(1) extra space?
"""
from typing import List


class Solution:
    def wiggleSort(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        nums.sort()
        pos = len(nums) // 2 if len(nums) % 2 == 0 else len(nums)//2 + 1
        smalls = nums[0:pos][::-1]
        larges = nums[pos:][::-1]
        j = 0
        k = 0
        for i in range(len(nums)):
            if i % 2 == 0:
                nums[i] = smalls[j]
                j += 1
            else:
                nums[i] = larges[k]
                k += 1


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [1, 3, 2, 2, 3, 1]
    print(len(ss))
    s_t = timeit.default_timer()
    s.wiggleSort(ss)
    print(ss)
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
