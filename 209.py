# -*- coding: utf-8 -*-
"""
Given an array of n positive integers and a positive integer s,
find the minimal length of a contiguous subarray of which the sum ≥ s.
If there isn't one, return 0 instead.

Example:

Input: s = 7, nums = [2,3,1,2,4,3]
Output: 2
Explanation: the subarray [4,3] has the minimal length under the problem constraint.
Follow up:
If you have figured out the O(n) solution,
try coding another solution of which the time complexity is O(n log n).
"""
from typing import List


class Solution:
    def minSubArrayLen(self, s: int, nums: List[int]) -> int:
        def _find_right_index(left_index, sums):
            low, high = left_index, len(sums) - 1
            sum_until_left = 0 if left_index - 1 < 0 else sums[left_index - 1]
            while low < high:
                mid = (low + high) // 2
                sum_until_mid = sums[mid]
                sum_span = sum_until_mid - sum_until_left
                if sum_span >= s:
                    high = mid
                else:
                    low = mid + 1
            right_index = low
            sum_until_right = sums[right_index]
            sum_span = sum_until_right - sum_until_left
            if sum_span >= s:
                return right_index
            else:
                return -1
        #
        if len(nums) == 0:
            return 0
        sums = [0] * len(nums)
        sums[0] = nums[0]
        for i in range(1, len(nums)):
            sums[i] = sums[i-1] + nums[i]
        #
        best = None
        for left_index in range(len(nums)):
            right_index = _find_right_index(left_index, sums)
            if -1 == right_index:
                continue
            if best is None:
                best = (nums[left_index: right_index + 1], right_index - left_index + 1)
            else:
                length = right_index - left_index + 1
                if length < best[1]:
                    best = (nums[left_index: right_index + 1], length)
        if best is None:
            return 0
        else:
            print(sum(best[0]))
            return best


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = 213
    nums = [12,28,83,4,25,26,25,2,25,25,25,12]
    s_t = timeit.default_timer()
    print(s.minSubArrayLen(ss, nums))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
