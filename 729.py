"""
Implement a MyCalendar class to store your events.
A new event can be added if adding the event will not cause a double booking.

Your class will have the method, book(int start, int end).
Formally, this represents a booking on the half open interval [start, end), the range of real numbers x such that start <= x < end.

A double booking happens when two events have some non-empty intersection (ie., there is some time that is common to both events.)

For each call to the method MyCalendar.book,
return true if the event can be added to the calendar successfully without causing a double booking.
Otherwise, return false and do not add the event to the calendar.

Your class will be called like this: MyCalendar cal = new MyCalendar(); MyCalendar.book(start, end)
Example 1:

MyCalendar();
MyCalendar.book(10, 20); // returns true
MyCalendar.book(15, 25); // returns false
MyCalendar.book(20, 30); // returns true
Explanation:
The first event can be booked.  The second can't because time 15 is already booked by another event.
The third event can be booked, as the first event takes every time less than 20, but not including 20.


Note:

The number of calls to MyCalendar.book per test case will be at most 1000.
In calls to MyCalendar.book(start, end), start and end are integers in the range [0, 10^9].
"""


class Node:
    def __init__(self, span):
        self.span = span
        self.left = None
        self.right = None


class TreeRecords:
    def __init__(self, root_record):
        self.root = Node(root_record)

    def insert(self, new_record):
        def _compare(record_a, record_b):
            if record_a.span[1] <= record_b.span[0]:
                return -1
            elif record_a.span[0] >= record_b.span[1]:
                return 1
            else:
                return 0
        #
        new_record = Node(new_record)
        # 插入新的记录
        if not self.root:
            self.root = new_record
            return True
        curr_record = self.root
        last_record = None
        where_insert = "none"
        while curr_record:
            last_record = curr_record
            if 1 == _compare(new_record, curr_record):
                where_insert = "right"
                curr_record = curr_record.right
            elif -1 == _compare(new_record, curr_record):
                where_insert = "left"
                curr_record = curr_record.left
            else:
                where_insert = "none"
                break
        if "left" == where_insert:
            last_record.left = new_record
            return True
        elif "right" == where_insert:
            last_record.right = new_record
            return True
        else:
            return False


class MyCalendar:

    def __init__(self):
        self.tree_record = None

    def book(self, start: int, end: int) -> bool:
        if not self.tree_record:
            self.tree_record = TreeRecords((start, end))
            return True
        else:
            return self.tree_record.insert((start, end))


if __name__ == "__main__":
    import timeit
    obj = MyCalendar()
    s_t = timeit.default_timer()
    for start, end in [[10,20],[15,25],[20,30]]:
        print(obj.book(start, end))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))