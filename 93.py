# -*- coding: utf-8 -*-
"""
Given a string containing only digits,
restore it by returning all possible valid IP address combinations.

Example:

Input: "25525511135"
Output: ["255.255.11.135", "255.255.111.35"]
"""
from typing import List


class Solution:
    def restoreIpAddresses(self, s: str) -> List[str]:
        def _is_legal(n):
            if "0" == n[0] and len(n) > 1:
                return False
            elif 0 <= int(n) <= 255:
                return True
            else:
                return False
        #
        if len(s) == 0:
            return []
        else:
            stack = [([], s[0:])]
            result = []
            while stack:
                ip, remain = stack.pop()
                curr_combs = [(ip + [remain[0:i]], remain[i:]) for i in range(1, 4)
                              if i < len(remain) and _is_legal(remain[0:i])]
                for comb in curr_combs:
                    _ip, _remain = comb
                    if len(_ip) < 3:
                        stack.append(comb)
                    elif len(_ip) == 3:
                        if not _is_legal(_remain):
                            continue
                        else:
                            result.append(".".join(_ip + [_remain]))
                    else:
                        pass
            return result


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "1111"
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.restoreIpAddresses(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
