# -*- coding: utf-8 -*-
"""
All DNA is composed of a series of nucleotides abbreviated as A, C, G, and T,
for example: "ACGAATTCCG". When studying DNA,
it is sometimes useful to identify repeated sequences within the DNA.

Write a function to find all the 10-letter-long sequences (substrings)
that occur more than once in a DNA molecule.

Example:

Input: s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT"

Output: ["AAAAACCCCC", "CCCCCAAAAA"]
"""
from typing import List


class Solution:
    def findRepeatedDnaSequences(self, s: str) -> List[str]:
        count = dict()
        result = set()
        for i in range(len(s) - 9):
            sub_str = s[i:i+10]
            if sub_str in count:
                if not count[sub_str]:
                    result.add(sub_str)
                    count[sub_str] = True
                else:
                    pass
            else:
                count[sub_str] = False
        return list(result)


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT"
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.findRepeatedDnaSequences(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
