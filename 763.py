"""
A string S of lowercase letters is given.
We want to partition this string into as many parts as possible
so that each letter appears in at most one part,
and return a list of integers representing the size of these parts.

Example 1:
Input: S = "ababcbacadefegdehijhklij"
Output: [9,7,8]
Explanation:
The partition is "ababcbaca", "defegde", "hijhklij".
This is a partition so that each letter appears in at most one part.
A partition like "ababcbacadefegde", "hijhklij" is incorrect, because it splits S into less parts.
Note:

S will have length in range [1, 500].
S will consist of lowercase letters ('a' to 'z') only.
"""


class Solution:
    def partitionLabels(self, S):
        if len(S) < 2:
            return len(S)
        right = dict([(c, i) for i, c in enumerate(S)])
        #
        result = [
            {
                "start_index": 0,
                "end_index": right[S[0]],
                "elements": [S[0]],
                "is_full": True if 0 == right[S[0]] else False
            }
        ]
        for i in range(1, len(S)):
            item = result[-1]
            if item["is_full"]:
                result.append(dict([
                    ("start_index", item["end_index"]+1),
                    ("end_index", right[S[i]]),
                    ("elements", [S[i]]),
                    ("is_full", True if i == right[S[i]] else False)
                ]))
            else:
                item["elements"].append(S[i])
                if right[S[i]] > item["end_index"]:
                    item["end_index"] = right[S[i]]
                if len(item["elements"]) == (item["end_index"]-item["start_index"]+1):
                    item["is_full"] = True
        #
        return [len(x["elements"]) for x in result]


import timeit
s = Solution()
S = "ababcbacadefegdehijhklij"
s_t = timeit.default_timer()
print(s.partitionLabels(S))
e_t = timeit.default_timer()
print("time cost: {:.5f} seconds.".format(e_t - s_t))

