# -*- coding: utf-8 -*-
"""
Given a collection of candidate numbers (candidates) and a target number (target),
find all unique combinations in candidates where the candidate numbers sums to target.

Each number in candidates may only be used once in the combination.

Note:

All numbers (including target) will be positive integers.
The solution set must not contain duplicate combinations.
Example 1:

Input: candidates = [10,1,2,7,6,1,5], target = 8,
A solution set is:
[
  [1, 7],
  [1, 2, 5],
  [2, 6],
  [1, 1, 6]
]
Example 2:

Input: candidates = [2,5,2,1,2], target = 5,
A solution set is:
[
  [1,2,2],
  [5]
]
"""
from typing import List


class Solution:
    def combinationSum2(self, candidates: List[int], target: int) -> List[List[int]]:
        length = len(candidates)
        if 0 == length:
            return [[]]
        candidates = sorted(candidates)
        # (path, remain, target)
        stack = [([], [i for i in range(length)], target)]
        result = set()
        while stack:
            curr_path, curr_remain_indexes, curr_target = stack.pop()
            for i in curr_remain_indexes:
                if candidates[i] < curr_target:
                    stack.append((curr_path + [i],
                                  [x for x in curr_remain_indexes if x not in curr_path + [i]],
                                  curr_target-candidates[i]))
                elif candidates[i] == curr_target:
                    result.add(str(sorted([candidates[j] for j in curr_path + [i]])))
                else:
                    break
        result = [eval(item) for item in list(result)]
        return result


if __name__ == "__main__":
    import timeit
    s = Solution()
    candidates = [23,16,5,28,20,17,27,20,12,21,12,29,12,20,22,32,7,26,20,30,28,17,7,26,5,24,20,27,5,8,9,15,19,31,9,5,13,31,12,20,10,11,6,17,27,16,7,21,7,22,16,30,26,10,21,27,29,28,11,21]
    target = 27
    s_t = timeit.default_timer()
    print(s.combinationSum2(candidates, target))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
