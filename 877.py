# -*- coding: utf-8 -*-
"""
Alex and Lee play a game with piles of stones.  There are an even number of piles arranged in a row,
and each pile has a positive integer number of stones piles[i].

The objective of the game is to end with the most stones.
The total number of stones is odd, so there are no ties.

Alex and Lee take turns, with Alex starting first.
Each turn, a player takes the entire pile of stones from either the beginning or the end of the row.
This continues until there are no more piles left, at which point the person with the most stones wins.

Assuming Alex and Lee play optimally, return True if and only if Alex wins the game.



Example 1:

Input: [5,3,4,5]
Output: true
Explanation:
Alex starts first, and can only take the first 5 or the last 5.
Say he takes the first 5, so that the row becomes [3, 4, 5].
If Lee takes 3, then the board is [4, 5], and Alex takes 5 to win with 10 points.
If Lee takes the last 5, then the board is [3, 4], and Alex takes 4 to win with 9 points.
This demonstrated that taking the first 5 was a winning move for Alex, so we return true.


Note:

2 <= piles.length <= 500
piles.length is even.
1 <= piles[i] <= 500
sum(piles) is odd.
"""
from typing import List


class Solution:
    def stoneGame(self, piles: List[int]) -> bool:
        dp = [[0 for _ in range(len(piles))] for _ in range(len(piles))]
        for i in range(len(piles)):
            dp[i][i] = piles[i]
        for delta in range(1, len(piles)):
            for i in range(len(piles)-1):
                if i + delta >= len(piles):
                    break
                dp[i][i+delta] = max(piles[i] - dp[i+1][i+delta], piles[i+delta] - dp[i][i+delta-1])
        return dp[0][len(piles)-1] > 0


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [5,3,4,5]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.stoneGame(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
