# -*- coding: utf-8 -*-
"""
Given an array with n objects colored red, white or blue,
sort them in-place so that objects of the same color are adjacent,
with the colors in the order red, white and blue.

Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.

Note: You are not suppose to use the library's sort function for this problem.

Example:

Input: [2,0,2,1,1,0]
Output: [0,0,1,1,2,2]

Follow up:

    A rather straight forward solution is a two-pass algorithm using counting sort.
    First, iterate the array counting number of 0's, 1's, and 2's,
    then overwrite array with total number of 0's,
    then 1's and followed by 2's.
    Could you come up with a one-pass algorithm using only constant space?
"""
from typing import List


class Solution:
    def sortColors(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        red = 0
        blue = len(nums) - 1
        i = 0
        while i <= blue:
            if 0 == nums[i]:
                nums[red], nums[i] = nums[i], nums[red]
                red += 1
                i += 1
            elif 2 == nums[i]:
                nums[blue], nums[i] = nums[i], nums[blue]
                blue -= 1
            else:
                i += 1


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [2,0,2,1,1,2,1,0,0,2,2,2,1,0,0]
    print(len(ss))
    s_t = timeit.default_timer()
    s.sortColors(ss)
    print(ss)
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
