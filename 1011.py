"""
A conveyor belt has packages that must be shipped from one port to another within D days.

The i-th package on the conveyor belt has a weight of weights[i].
Each day, we load the ship with packages on the conveyor belt (in the order given by weights).
We may not load more weight than the maximum weight capacity of the ship.

Return the least weight capacity of the ship
that will result in all the packages on the conveyor belt being shipped within D days.


Example 1:

Input: weights = [1,2,3,4,5,6,7,8,9,10], D = 5
Output: 15
Explanation:
A ship capacity of 15 is the minimum to ship all the packages in 5 days like this:
1st day: 1, 2, 3, 4, 5
2nd day: 6, 7
3rd day: 8
4th day: 9
5th day: 10

Note that the cargo must be shipped in the order given, so using a ship of capacity 14 and splitting the packages into parts like (2, 3, 4, 5), (1, 6, 7), (8), (9), (10) is not allowed.
Example 2:

Input: weights = [3,2,2,4,1,4], D = 3
Output: 6
Explanation:
A ship capacity of 6 is the minimum to ship all the packages in 3 days like this:
1st day: 3, 2
2nd day: 2, 4
3rd day: 1, 4
Example 3:

Input: weights = [1,2,3,1,1], D = 4
Output: 3
Explanation:
1st day: 1
2nd day: 2
3rd day: 3
4th day: 1, 1


Note:

1 <= D <= weights.length <= 50000
1 <= weights[i] <= 500
"""


class Solution:
    def shipWithinDays(self, weights, D) -> int:
        if D == len(weights):
            return max(weights)

        #
        def is_ok(weights, D, curr_capacity):
            curr_sum = 0
            days = 0
            is_changed = False
            for i in range(len(weights)):
                is_changed = False
                curr_sum += weights[i]
                if curr_sum > curr_capacity:
                    curr_sum = weights[i]
                    days += 1
                    is_changed = True
            if not is_changed or curr_sum == weights[-1]:
                days += 1
            return days <= D
        #
        max_capacity = sum(weights)
        min_capacity = max(weights)
        while min_capacity < max_capacity:
            curr_capacity = (min_capacity + max_capacity) // 2
            if is_ok(weights, D, curr_capacity):
                max_capacity = curr_capacity
            else:
                min_capacity = curr_capacity + 1
        return max_capacity


import timeit
s = Solution()
weights = [1,2,3,4,5,6,7,8,9,10]
D = 1
print(len(weights))
s_t = timeit.default_timer()
print(s.shipWithinDays(weights, D))
e_t = timeit.default_timer()
print("time cost: {:.5f} seconds.".format(e_t - s_t))
