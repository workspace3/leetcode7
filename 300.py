"""
给定一个无序的整数数组，找到其中最长上升子序列的长度。

示例:

输入: [10,9,2,5,3,7,101,18]
输出: 4
解释: 最长的上升子序列是 [2,3,7,101]，它的长度是 4。
说明:

可能会有多种最长上升子序列的组合，你只需要输出对应的长度即可。
你算法的时间复杂度应该为 O(n2) 。
进阶: 你能将算法的时间复杂度降低到 O(n log n) 吗?

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/longest-increasing-subsequence
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
"""


class Solution:
    def lengthOfLIS(self, nums) -> int:
        # dp[i]表示以nums[i]结尾的最长子串的长度
        length = len(nums)
        if 0 == length:
            return 0
        dp = [1]*length
        for i in range(1, length):
            for j in range(i):
                if nums[j] < nums[i]:
                    dp[i] = max(dp[i], dp[j]+1)
        #
        return sorted(dp)[-1]


if __name__ == "__main__":
    import timeit
    s = Solution()
    # nums = [2,4,3,10,3,2,4,4,9,3,5,8,8,9,3,0,4,3,3,9,4,3,4,1,9,7,2,9,1,9,10,5,5,5,5,3,3,10,9,3,7,6,6,2,7,7,9,8,3,7,2,4,4,9,4,5,10,7,-1,0,5,1,9,4,2,3,0,5,0,2,8,1,0,7,10,4,8,3,6,0,4,3,3,8,4]
    nums = [1,3,6,7,9,4,10,5,6]
    print(len(nums))
    s_t = timeit.default_timer()
    print(s.lengthOfLIS(nums))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))