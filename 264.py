# -*- coding: utf-8 -*-
"""
Write a program to find the n-th ugly number.

Ugly numbers are positive numbers whose prime factors only include 2, 3, 5.

Example:

Input: n = 10
Output: 12
Explanation: 1, 2, 3, 4, 5, 6, 8, 9, 10, 12 is the sequence of the first 10 ugly numbers.
Note:

1 is typically treated as an ugly number.
n does not exceed 1690.
"""


class Solution:
    def nthUglyNumber(self, n: int) -> int:
        factors = [2, 3, 5]
        indexes = [0, 0, 0]
        dp = [1]
        while len(dp) < n:
            curr_values = [f*dp[i] for f, i in zip(factors, indexes)]
            min_value = min(curr_values)
            min_index = curr_values.index(min_value)
            indexes[min_index] += 1
            if min_value != dp[-1]:
                dp.append(min_value)
        return dp[-1]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = 1690
    s_t = timeit.default_timer()
    print(s.nthUglyNumber(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
