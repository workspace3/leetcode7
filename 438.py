# -*- coding: utf-8 -*-
"""
Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.

Strings consists of lowercase English letters only
and the length of both strings s and p will not be larger than 20,100.

The order of output does not matter.

Example 1:

Input:
s: "cbaebabacd" p: "abc"

Output:
[0, 6]

Explanation:
The substring with start index = 0 is "cba", which is an anagram of "abc".
The substring with start index = 6 is "bac", which is an anagram of "abc".
Example 2:

Input:
s: "abab" p: "ab"

Output:
[0, 1, 2]

Explanation:
The substring with start index = 0 is "ab", which is an anagram of "ab".
The substring with start index = 1 is "ba", which is an anagram of "ab".
The substring with start index = 2 is "ab", which is an anagram of "ab".
"""
from typing import List


class Solution:
    def findAnagrams(self, s: str, p: str) -> List[int]:
        s_len = len(s)
        p_len = len(p)
        p_sorted = "".join(sorted(p))
        if s_len < p_len:
            return []
        count = {}
        for k in range(p_len):
            if s[k] not in count:
                count[s[k]] = 1
            else:
                count[s[k]] += 1
        #
        indexes = []
        curr_sub_s = "".join([x*count[x] for x in sorted(count.keys())])
        if curr_sub_s == p_sorted:
            indexes.append(0)
        for i in range(1, s_len - p_len + 1):
            count[s[i-1]] -= 1
            if s[i+p_len-1] not in count:
                count[s[i+p_len-1]] = 1
            else:
                count[s[i+p_len-1]] += 1
            curr_sub_s = "".join([x * count[x] for x in sorted(count.keys())])
            if curr_sub_s == p_sorted:
                indexes.append(i)
        return indexes


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "cbaebabacd"
    p = "ab"
    s_t = timeit.default_timer()
    print(s.findAnagrams(ss, p))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))

