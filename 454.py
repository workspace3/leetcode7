# -*- coding: utf-8 -*-
"""
Given four lists A, B, C, D of integer values,
compute how many tuples (i, j, k, l) there are such that A[i] + B[j] + C[k] + D[l] is zero.

To make problem a bit easier, all A, B, C, D have same length of N where 0 ≤ N ≤ 500.
All integers are in the range of -228 to 228 - 1
and the result is guaranteed to be at most 231 - 1.

Example:

Input:
A = [ 1, 2]
B = [-2,-1]
C = [-1, 2]
D = [ 0, 2]

Output:
2

Explanation:
The two tuples are:
1. (0, 0, 0, 1) -> A[0] + B[0] + C[0] + D[1] = 1 + (-2) + (-1) + 2 = 0
2. (1, 1, 0, 0) -> A[1] + B[1] + C[0] + D[0] = 2 + (-1) + (-1) + 0 = 0
-------其他解法-------
class Solution(object):
    def fourSumCount(self, A, B, C, D):
        d={}
        for i in range(len(A)):
            for j in range(len(B)):
                s= A[i] + B[j]
                if s in d:
                    d[s]+=1
                else:
                    d[s]=1

        count=0
        for i in range(len(C)):
            for j in range(len(D)):
                s= C[i] + D[j]
                if -s in d:
                    count+= d[-s]
        return count
"""
from typing import List
from collections import Counter


class Solution:
    def fourSumCount(self, A: List[int], B: List[int], C: List[int], D: List[int]) -> int:
        if any([True if len(x) == 0 else False for x in [A, B, C, D]]):
            return 0
        count_a = Counter(A)
        count_b = Counter(B)
        curr_sum = {}
        for k1 in count_a:
            for k2 in count_b:
                tmp_sum = k1 + k2
                if tmp_sum not in curr_sum:
                    curr_sum[tmp_sum] = 0
                curr_sum[tmp_sum] += (count_a[k1] * count_b[k2])
        count_c = Counter(C)
        count_d = Counter(D)
        result = 0
        for k3 in count_c:
            for k4 in count_d:
                tmp_sum = k3 + k4
                if -tmp_sum in curr_sum:
                    result += count_c[k3]*count_d[k4]*curr_sum[-tmp_sum]
        return result


if __name__ == "__main__":
    import timeit
    s = Solution()
    A = [1, 2]
    B = [-2, -1]
    C = [-1, 2]
    D = [0, 2]
    s_t = timeit.default_timer()
    print(s.fourSumCount(A, B, C, D))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
