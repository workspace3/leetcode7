# -*- coding: utf-8 -*-
"""
The n-queens puzzle is the problem of placing n queens on an n×n chessboard
such that no two queens attack each other.



Given an integer n, return the number of distinct solutions to the n-queens puzzle.

Example:

Input: 4
Output: 2
Explanation: There are two distinct solutions to the 4-queens puzzle as shown below.
[
 [".Q..",  // Solution 1
  "...Q",
  "Q...",
  "..Q."],

 ["..Q.",  // Solution 2
  "Q...",
  "...Q",
  ".Q.."]
]
"""


class Solution:
    count = 0

    def totalNQueens(self, n: int) -> int:
        self.n = n
        track = [[0 for _ in range(n)] for _ in range(n)]

        #
        def is_valid(x, y):
            for i in range(n):
                for j in range(n):
                    if track[i][j] and (x == i or y == j):
                        return False
                    elif track[i][j] and abs(x-i) == abs(y-j):
                        return False
            return True
        #

        def helper(row):
            if row == self.n:
                self.count += 1
                return
            for col in range(self.n):
                if is_valid(row, col):
                    track[row][col] = 1
                    helper(row + 1)
                    track[row][col] = 0
        #
        helper(0)
        return self.count


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = 8
    s_t = timeit.default_timer()
    print(s.totalNQueens(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
