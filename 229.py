# -*- coding: utf-8 -*-
"""
Given an integer array of size n, find all elements that appear more than ⌊ n/3 ⌋ times.

Note: The algorithm should run in linear time and in O(1) space.

Example 1:

Input: [3,2,3]
Output: [3]

Example 2:

Input: [1,1,1,3,3,2,2,2]
Output: [1,2]

"""
from typing import List


class Solution:
    def majorityElement(self, nums: List[int]) -> List[int]:
        threshold = len(nums) // 3
        record = {}
        for n in nums:
            if n in record:
                record[n] += 1
            else:
                record[n] = 1
        return [k for k in record if record[k] > threshold]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [3, 2, 3]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.majorityElement(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
