# -*- coding: utf-8 -*-
"""
There is a brick wall in front of you.
The wall is rectangular and has several rows of bricks.
The bricks have the same height but different width.
You want to draw a vertical line from the top to the bottom and cross the least bricks.

The brick wall is represented by a list of rows.
Each row is a list of integers
representing the width of each brick in this row from left to right.

If your line go through the edge of a brick,
then the brick is not considered as crossed.
You need to find out how to draw the line to cross the least bricks
and return the number of crossed bricks.

You cannot draw a line just along one of the two vertical edges of the wall,
in which case the line will obviously cross no bricks.



Example:

Input: [[1,2,2,1],
        [3,1,2],
        [1,3,2],
        [2,4],
        [3,1,2],
        [1,3,1,1]]

Output: 2

Explanation:



Note:

The width sum of bricks in different rows are the same and won't exceed INT_MAX.
The number of bricks in each row is in range [1,10,000].
The height of wall is in range [1,10,000].
Total number of bricks of the wall won't exceed 20,000.
"""
from typing import List
from collections import Counter


class Solution:
    def leastBricks(self, wall: List[List[int]]) -> int:
        height = len(wall)
        store = []
        for i in range(height):
            curr_store = []
            curr_sum = 0
            for brick in wall[i][0:-1]:
                curr_sum += brick
                curr_store.append(curr_sum)
            store += curr_store
        if len(store) == 0:
            return height
        else:
            count = Counter(store)
            return height - max(count.values())


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [[1,2,2,1],
        [3,1,2],
        [1,3,2],
        [2,4],
        [3,1,2],
        [1,3,1,1]]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.leastBricks(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
