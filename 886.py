"""
Given a set of N people (numbered 1, 2, ..., N), we would like to split everyone into two groups of any size.

Each person may dislike some other people, and they should not go into the same group.

Formally, if dislikes[i] = [a, b], it means it is not allowed to put the people numbered a and b into the same group.

Return true if and only if it is possible to split everyone into two groups in this way.



Example 1:

Input: N = 4, dislikes = [[1,2],[1,3],[2,4]]
Output: true
Explanation: group1 [1,4], group2 [2,3]
Example 2:

Input: N = 3, dislikes = [[1,2],[1,3],[2,3]]
Output: false
Example 3:

Input: N = 5, dislikes = [[1,2],[2,3],[3,4],[4,5],[1,5]]
Output: false


Note:

1 <= N <= 2000
0 <= dislikes.length <= 10000
1 <= dislikes[i][j] <= N
dislikes[i][0] < dislikes[i][1]
There does not exist i != j for which dislikes[i] == dislikes[j].
"""
from typing import List


class Solution:
    def possibleBipartition(self, N: int, dislikes: List[List[int]]) -> bool:
        from copy import deepcopy
        if len(dislikes) == 0:
            return True
        groups = [{dislikes[0][0]: 0, dislikes[0][1]: 1}]
        for left, right in dislikes[1:]:
            del_groups = []
            is_found = False
            for group in groups:
                if left in group and right in group:
                    is_found = True
                    if group[left] == group[right]:
                        del_groups.append(group)
                elif left in group and right not in group:
                    is_found = True
                    group[right] = 0 if 1 == group[left] else 1
                elif left not in group and right in group:
                    is_found = True
                    group[left] = 0 if 1 == group[right] else 1
                else:
                    continue
            groups = [g for g in groups if g not in del_groups]
            if len(groups) == 0:
                return False
            if not is_found:
                branches = []
                for g in groups:
                    branches.append(deepcopy(g))
                    branches[-1][left] = 0
                    branches[-1][right] = 1
                    g[left] = 1
                    g[right] = 0
                groups.extend(branches)
        return True


if __name__ == "__main__":
    import timeit
    s = Solution()
    N = 10
    dislikes = []
    s_t = timeit.default_timer()
    print(s.possibleBipartition(N, dislikes))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))


