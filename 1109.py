"""
There are n flights, and they are labeled from 1 to n.

We have a list of flight bookings.
The i-th booking bookings[i] = [i, j, k] means that we booked k seats from flights labeled i to j inclusive.

Return an array answer of length n,
representing the number of seats booked on each flight in order of their label.

Example 1:

Input: bookings = [[1,2,10],[2,3,20],[2,5,25]], n = 5
Output: [10,55,45,25,25]
-----------------------
这里有 n 个航班，它们分别从 1 到 n 进行编号。

我们这儿有一份航班预订表，表中第 i 条预订记录 bookings[i] = [i, j, k] 
意味着我们在从 i 到 j 的每个航班上预订了 k 个座位。

请你返回一个长度为 n 的数组 answer，按航班编号顺序返回每个航班上预订的座位数。

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/corporate-flight-bookings
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
"""


class Solution:
    def corpFlightBookings(self, bookings, n: int):
        result = [0]*(n+1)
        for b in bookings:
            s,e,num = b
            for i in range(s, e+1):
                result[i] += num
        return result[1:]


if __name__ == "__main__":
    import timeit
    s = Solution()
    bookings = [[1,2,10],[2,3,20],[2,5,25]]
    n = 5
    s_t = timeit.default_timer()
    print(s.corpFlightBookings(bookings, n))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))