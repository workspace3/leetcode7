# -*- coding: utf-8 -*-
"""
Given an integer array arr and an integer difference,
return the length of the longest subsequence in arr which is an arithmetic sequence
such that the difference between adjacent elements in the subsequence equals difference.

Example 1:

Input: arr = [1,2,3,4], difference = 1
Output: 4
Explanation: The longest arithmetic subsequence is [1,2,3,4].
Example 2:

Input: arr = [1,3,5,7], difference = 1
Output: 1
Explanation: The longest arithmetic subsequence is any single element.
Example 3:

Input: arr = [1,5,7,8,5,3,4,2,1], difference = -2
Output: 4
Explanation: The longest arithmetic subsequence is [7,5,3,1].


Constraints:

1 <= arr.length <= 10^5
-10^4 <= arr[i], difference <= 10^4
"""
from typing import List
from copy import deepcopy


class Solution:
    def longestSubsequence(self, arr: List[int], difference: int) -> int:
        dp = [1 for _ in range(len(arr))]
        max_len = 1
        for i in range(1, len(arr)):
            for j in range(i):
                if arr[i] - arr[j] == difference:
                    dp[i] = max(dp[i], dp[j] + 1)
                    if dp[i] > max_len:
                        max_len = dp[i]
        return max_len


if __name__ == "__main__":
    import timeit
    s = Solution()
    arr = [3,4,-3,-2,-4]
    difference = -5
    s_t = timeit.default_timer()
    print(s.longestSubsequence(arr, difference))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
