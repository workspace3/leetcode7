# -*- coding: utf-8 -*-
"""
Given a triangle, find the minimum path sum from top to bottom.
Each step you may move to adjacent numbers on the row below.

For example, given the following triangle

[
     [2],
    [3,4],
   [6,5,7],
  [4,1,8,3]
]
The minimum path sum from top to bottom is 11 (i.e., 2 + 3 + 5 + 1 = 11).

Note:

Bonus point if you are able to do this using only O(n) extra space,
where n is the total number of rows in the triangle.
"""
from typing import List


class Solution:
    def minimumTotal(self, triangle: List[List[int]]) -> int:
        layers = len(triangle)
        dp = [triangle[i][0:] for i in range(layers)]
        for i in range(1, layers):
            length = len(triangle[i])
            for j in range(length):
                if 0 == j:
                    dp[i][j] += dp[i-1][j]
                elif length-1 == j:
                    dp[i][j] += dp[i-1][j-1]
                else:
                    dp[i][j] += min(dp[i-1][j], dp[i-1][j-1])
        return min(dp[-1])


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [[-1],
          [2,3],
          [1,-1,-3]]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.minimumTotal(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))

