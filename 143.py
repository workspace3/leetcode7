# -*- coding: utf-8 -*-
"""
Given a singly linked list L: L0→L1→…→Ln-1→Ln,
reorder it to: L0→Ln→L1→Ln-1→L2→Ln-2→…

You may not modify the values in the list's nodes, only nodes itself may be changed.

Example 1:

Given 1->2->3->4, reorder it to 1->4->2->3.
Example 2:

Given 1->2->3->4->5, reorder it to 1->5->2->4->3.
"""


# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

    def printout(self):
        result = [self.val]
        next_node = self.next
        while next_node:
            result.append(next_node.val)
            next_node = next_node.next
        print(" -> ".join(map(str, result)))


class Solution:
    def reorderList(self, head: ListNode) -> None:
        """
        Do not return anything, modify head in-place instead.
        """
        if head is None:
            return
        d = [head]
        next_node = head.next
        while next_node:
            d.append(next_node)
            next_node = next_node.next
        #
        left, right = 0, len(d) - 1
        i = 0
        while left < right:
            if i % 2 == 0:
                d[left].next = d[right]
                left += 1
            else:
                d[right].next = d[left]
                right -= 1
            i += 1
        if i % 2 == 1:
            d[right].next = None
        else:
            d[left].next = None


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [ListNode(i) for i in range(1, 6)]
    for i in range(0, len(ss) - 1):
        ss[i].next = ss[i+1]
    ss[0].printout()
    s_t = timeit.default_timer()
    s.reorderList(ss[0])
    ss[0].printout()
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))

