# -*- coding: utf-8 -*-
"""
Implement a basic calculator to evaluate a simple expression string.

The expression string contains only non-negative integers, +, -, *, / operators and empty spaces .
The integer division should truncate toward zero.

Example 1:

Input: "3+2*2-1*2+2"
Output: 7

Example 2:

Input: " 3/2 "
Output: 1

Example 3:

Input: " 3+5 / 2 "
Output: 5

Note:

    You may assume that the given expression is always valid.
    Do not use the eval built-in library function.
"""


class Solution:
    def calculate(self, s: str) -> int:
        # --简单做法--
        # return int(eval(s.replace(" ", "")))
        # ----------
        def _is_higher_than(x, y):
            _priority = {"*": 1,
                         "/": 1,
                         "-": 0,
                         "+": 0}
            priority_x = _priority[x]
            priority_y = _priority[y]
            return priority_x > priority_y
        # 1) 转为后缀表达式
        stack_opera, stack_tmp_result = [], []
        curr_number = ""
        for item in s:
            if " " == item:
                continue
            if item.isdigit():
                curr_number += item
            else:
                stack_tmp_result.append(int(curr_number))
                curr_number = ""
                while len(stack_opera) > 0 and (not _is_higher_than(item, stack_opera[-1])):
                    stack_tmp_result.append(stack_opera.pop())
                stack_opera.append(item)
        if len(curr_number) > 0:
            stack_tmp_result.append(int(curr_number))
        if len(stack_opera) > 0:
            stack_tmp_result += stack_opera[-1::-1]
        print(stack_tmp_result)
        # 2) 计算表达式的值
        stack = []
        for item in stack_tmp_result:
            if isinstance(item, int):
                stack.append(item)
            else:
                right, left = stack.pop(), stack.pop()
                if "+" == item:
                    curr_result = left + right
                    stack.append(curr_result)
                elif "-" == item:
                    curr_result = left - right
                    stack.append(curr_result)
                elif "*" == item:
                    curr_result = int(left * right)
                    stack.append(curr_result)
                elif "/" == item:
                    curr_result = int(left / right)
                    stack.append(curr_result)
                else:
                    pass
        return stack[0]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "1-1-1"
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.calculate(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
