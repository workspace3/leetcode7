# -*- coding: utf-8 -*-
"""
Implement next permutation,
which rearranges numbers into the lexicographically next greater permutation of numbers.

If such arrangement is not possible,
it must rearrange it as the lowest possible order (ie, sorted in ascending order).

The replacement must be in-place and use only constant extra memory.

Here are some examples.
Inputs are in the left-hand column and its corresponding outputs are in the right-hand column.

1,2,3 → 1,3,2
3,2,1 → 1,2,3
1,1,5 → 1,5,1
"""
from typing import List


class Solution:
    def nextPermutation(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        def _find_first_down(numbers):
            l = len(numbers)
            for i in range(l-1, -1, -1):
                if i+1 < l and numbers[i] < numbers[i+1]:
                    return i
            return -1

        #
        def _find_last_bigger(curr_pos, numbers):
            l = len(numbers)
            i = curr_pos + 1
            while i < l:
                if numbers[i] > numbers[curr_pos]:
                    pass
                else:
                    return i - 1
                i += 1
            if i == l:
                return l - 1
            else:
                return -1

        #
        def _swap(i, j):
            tmp = nums[i]
            nums[i] = nums[j]
            nums[j] = tmp
        #
        length = len(nums)
        if 0 == length:
            pass
        else:
            first_down_pos = _find_first_down(nums)
            if -1 == first_down_pos:
                nums[0:] = nums[-1::-1]
            else:
                first_bigger_pos = _find_last_bigger(first_down_pos, nums)
                if -1 == first_bigger_pos:
                    pass
                else:
                    _swap(first_down_pos, first_bigger_pos)
                    nums[first_down_pos+1:] = nums[-1:first_down_pos:-1]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [1,2,3]
    print(len(ss))
    s_t = timeit.default_timer()
    s.nextPermutation(ss)
    print(ss)
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
