# -*- coding: utf-8 -*-
"""
Given a non-empty array of integers, return the k most frequent elements.

Example 1:

Input: nums = [1,1,1,2,2,3], k = 2
Output: [1,2]
Example 2:

Input: nums = [1], k = 1
Output: [1]
Note:

You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
Your algorithm's time complexity must be better than O(n log n), where n is the array's size.
"""
from typing import List
from collections import Counter


class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        return [item[0] for item in sorted(Counter(nums).items(), key=lambda x: x[1], reverse=True)][0:k]


if __name__ == "__main__":
    import timeit
    s = Solution()
    nums = [1,1,1,2,2,3]
    k = 2
    s_t = timeit.default_timer()
    print(s.topKFrequent(nums, k))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
