# -*- coding: utf-8 -*-
"""
Given a collection of distinct integers, return all possible permutations.

Example:

Input: [1,2,3]
Output:
[
  [1,2,3],
  [1,3,2],
  [2,1,3],
  [2,3,1],
  [3,1,2],
  [3,2,1]
]

"""
from typing import List
import itertools


class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        result = itertools.permutations(nums)
        return [list(x) for x in result]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [1,2,3]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.permute(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))

