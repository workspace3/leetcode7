"""
Given string S and a dictionary of words words,
find the number of words[i] that is a subsequence of S.

Example :
Input:
S = "abcde"
words = ["a", "bb", "acd", "ace"]
Output: 3
Explanation: There are three words in words that are a subsequence of S: "a", "acd", "ace".
Note:

All words in words and S will only consists of lowercase letters.
The length of S will be in the range of [1, 50000].
The length of words will be in the range of [1, 5000].
The length of words[i] will be in the range of [1, 50].

----------------------------------
其他人思路：
The idea is to use the String S and build a dictionary of indexes for each character in the string.
Then for each word, we can go character by character and see if it is a subsequence,
by using the corresponding index dictionary,
but just making sure that the index of the current character occurs after the index where the previous character was seen.
To speed up the processing, we should use binary search in the index dictionary.

As an example for S = "aacbacbde"
the
dict_idxs =
{a: [0, 1, 4]
b: [3, 6]
c: [2, 5]
d: [7]
e: [8]
}
Now for the word say "abcb", starting with d_i = 0,
a => get list for a in the dict_idxs which is [0, 1, 4], and in the list,
    find the index of a >= d_i which is 0. After this update d_i to +1 => 1
b => get list for b in the dict_idxs [3, 6], and in the list,
    find the index of b >= d_i => >= 1, which is at index 0 => 3, after this update d_i to 4+1 => 4
c => in the list for c, [3, 5],
    find the index of c >= 4, which is 5. update d_i to 5+1 = 6
b => in the list for b, [3, 6],
    fund the index of b >= 6, which is 6, update d_i to 7, since this is the end of the word, and we have found all characters in the word in the index dictionary, we return True for this word.

    def numMatchingSubseq(self, S, words):
        def isMatch(word, w_i, d_i):
            if w_i == len(word): return True
            l = dict_idxs[word[w_i]]
            if len(l) == 0 or d_i > l[-1]: return False
            i = l[bisect_left(l, d_i)]
            return isMatch(word, w_i + 1, i + 1)

        dict_idxs = defaultdict(list)
        for i in range(len(S)):
            dict_idxs[S[i]].append(i)
        return sum(isMatch(word, 0, 0) for word in words)
"""


class Solution:
    def numMatchingSubseq(self, S: str, words) -> int:
        length_of_words = len(words)
        pointer = [0]*length_of_words
        count = 0
        for c in S:
            for i in range(length_of_words):
                if pointer[i] < len(words[i]):
                    if c == words[i][pointer[i]]:
                        pointer[i] += 1
                        if pointer[i] == len(words[i]):
                            count += 1
        return count


if __name__ == "__main__":
    import timeit
    s = Solution()
    S = "abcde"
    words = ["a", "bb", "acd", "ace"]
    s_t = timeit.default_timer()
    print(s.numMatchingSubseq(S, words))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
