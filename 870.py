# -*- coding: utf-8 -*-
"""
Given two arrays A and B of equal size,
the advantage of A with respect to B is the number of indices i for which A[i] > B[i].

Return any permutation of A that maximizes its advantage with respect to B.

Example 1:

Input: A = [2,7,11,15], B = [1,10,4,11]
Output: [2,11,7,15]
Example 2:

Input: A = [12,24,8,32], B = [13,25,32,11]
Output: [24,32,8,12]


Note:

1 <= A.length = B.length <= 10000
0 <= A[i] <= 10^9
0 <= B[i] <= 10^9
"""
from typing import List


class Solution:
    def advantageCount(self, A: List[int], B: List[int]) -> List[int]:
        def _find_closest_bigger(point, collection):
            low, high = 0, len(collection) - 1
            if point < collection[low] or point >= collection[high]:
                return 0
            while low < high:
                mid = (low + high) // 2
                if point < collection[mid]:
                    if mid - 1 >= 0 and collection[mid - 1] <= point:
                        return mid
                    else:
                        high = mid - 1
                elif point == collection[mid]:
                    if mid + 1 < len(collection):
                        return mid + 1
                    else:
                        return 0
                else:
                    if mid + 1 < len(collection) and collection[mid + 1] > point:
                        return mid + 1
                    else:
                        low = mid + 1
            return 0
        #
        if len(A) == 0:
            return []
        A = sorted(A)
        result = []
        for b in B:
            i = _find_closest_bigger(b, A)
            result.append(A.pop(i))
        return result


if __name__ == "__main__":
    import timeit
    s = Solution()
    A = [28, 47, 45, 8, 2, 10, 25, 35, 43, 37, 33, 30, 33, 20, 33, 42, 43, 36, 34, 3, 16, 23, 15, 10, 19, 42, 13, 47, 0, 21,
     36, 38, 0, 5, 3, 28, 4, 20, 14, 5, 19, 22, 29, 17, 3, 16, 35, 0, 26, 0]
    B = [44, 10, 27, 4, 27, 40, 46, 40, 45, 0, 41, 2, 44, 50, 36, 30, 37, 4, 44, 4, 12, 13, 35, 20, 19, 25, 38, 42, 43, 14,
     2, 4, 5, 38, 4, 38, 0, 35, 12, 32, 38, 33, 3, 1, 19, 46, 23, 13, 24, 41]
    s_t = timeit.default_timer()
    C = s.advantageCount(A, B)
    D = [35,15,29,13,28,47,35,45,33,3,47,4,30,23,37,33,38,10,22,10,16,19,36,21,20,28,43,0,0,19,3,8,14,43,5,42,2,36,16,33,42,34,5,3,20,0,25,17,26,0]
    c_b = [1 if C[i]>B[i] else 0 for i in range(len(A))]
    d_b = [1 if D[i]>B[i] else 0 for i in range(len(A))]
    print("my answer: {}, correct answer: {}".format(c_b.count(1), d_b.count(1)))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
