"""
Consider all the leaves of a binary tree.  From left to right order,
the values of those leaves form a leaf value sequence.

https://leetcode.com/problems/leaf-similar-trees/

For example, in the given tree above, the leaf value sequence is (6, 7, 4, 9, 8).

Two binary trees are considered leaf-similar if their leaf value sequence is the same.

Return true if and only if the two given trees with head nodes root1 and root2 are leaf-similar.
"""


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution:
    def leafSimilar(self, root1: TreeNode, root2: TreeNode) -> bool:
        if root1 is None and root2 is None:
            return True
        elif root1 is None and root2 is not None:
            return False
        elif root1 is not None and root2 is None:
            return False
        else:
            stack_1 = [root1]
            stack_2 = [root2]
            result_1 = []
            result_2 = []
            curr_pos = 0
            while stack_1 or stack_2:
                if stack_1:
                    curr_1 = stack_1.pop()
                    if curr_1.right is not None:
                        stack_1.append(curr_1.right)
                    else:
                        if curr_1.left is not None:
                            stack_1.append(curr_1.left)
                        else:
                            result_1.append(curr_1.val)
                            if curr_pos < len(result_1) and curr_pos < len(result_2):
                                if result_1[curr_pos] == result_2[curr_pos]:
                                    curr_pos += 1
                                else:
                                    return False
                if stack_2:
                    curr_2 = stack_2.pop()
                    if curr_2.right is not None:
                        stack_2.append(curr_2.right)
                    else:
                        if curr_2.left is not None:
                            stack_2.append(curr_2.left)
                        else:
                            result_2.append(curr_2.val)
                            if curr_pos < len(result_1) and curr_pos < len(result_2):
                                if result_1[curr_pos] == result_2[curr_pos]:
                                    curr_pos += 1
                                else:
                                    return False
            return True


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "asdsdsdsd"
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.leafSimilar(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))