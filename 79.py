# -*- coding: utf-8 -*-
"""
Given a 2D board and a word, find if the word exists in the grid.

The word can be constructed from letters of sequentially adjacent cell,
where "adjacent" cells are those horizontally or vertically neighboring.
The same letter cell may not be used more than once.

Example:

board =
[
  ['A','B','C','E'],
  ['S','F','C','S'],
  ['A','D','E','E']
]

Given word = "ABCCED", return true.
Given word = "SEE", return true.
Given word = "ABCB", return false.
"""
from typing import List


class Solution:
    def exist(self, board: List[List[str]], word: str) -> bool:
        def _find_starts():
            results = []
            rows = len(board)
            cols = len(board[0])
            for row in range(rows):
                indexes = [i for i in range(cols) if word[0] == board[row][i]]
                if len(indexes) > 0:
                    results.extend([(row, i) for i in indexes])
            return results

        def _is_inbound(x, y):
            if 0 <= x < len(board) and 0 <= y < len(board[0]):
                return True
            return False
        #
        if len(board) == 0 or len(board[0]) == 0 or len(word) == 0:
            return False
        #
        default_directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]
        all_start_points = _find_starts()
        for start_row, start_col in all_start_points:
            in_path = {str((start_row, start_col))}
            stack = [(start_row, start_col, default_directions[0:])]
            word_pos = 1
            while stack:
                curr_row, curr_col, directions = stack[-1]
                if len(word) == word_pos:
                    return True
                is_found = False
                for k in range(len(directions)):
                    d = directions[k]
                    next_row, next_col = curr_row + d[0], curr_col + d[1]
                    if _is_inbound(next_row, next_col) and \
                            word[word_pos] == board[next_row][next_col] and \
                            str((next_row, next_col)) not in in_path:
                        stack[-1] = (stack[-1][0], stack[-1][1], stack[-1][2][k+1:])
                        stack.append((next_row, next_col, default_directions[0:]))
                        in_path.add(str((next_row, next_col)))
                        word_pos += 1
                        is_found = True
                        break
                    else:
                        pass
                if not is_found:
                    last = stack.pop(-1)
                    in_path.remove(str((last[0], last[1])))
                    word_pos -= 1
        return False


if __name__ == "__main__":
    import timeit
    s = Solution()
    board = [
        ['A', 'B', 'C', 'E'],
        ['S', 'F', 'C', 'S'],
        ['A', 'D', 'E', 'E']
    ]
    word = "FCCESEEDASA"
    s_t = timeit.default_timer()
    print(s.exist(board, word))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
