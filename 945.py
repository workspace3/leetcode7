# -*- coding: utf-8 -*-
"""
Given an array of integers A, a move consists of choosing any A[i], and incrementing it by 1.

Return the least number of moves to make every value in A unique.



Example 1:

Input: [1,2,2]
Output: 1
Explanation:  After 1 move, the array could be [1, 2, 3].
Example 2:

Input: [3,2,1,2,1,7]
Output: 6
Explanation:  After 6 moves, the array could be [3, 4, 1, 2, 5, 7].
It can be shown with 5 or less moves that it is impossible for the array to have all unique values.


Note:

0 <= A.length <= 40000
0 <= A[i] < 40000
--------其他解法--------
Intuition

Let's count the quantity of each element. Clearly, we want to increment duplicated values.

For each duplicate value, we could do a "brute force" solution of incrementing it repeatedly until it is not unique.
However, we might do a lot of work - consider the work done by an array of all ones.
We should think of how to amend our solution to solve this case as well.

What we can do instead is lazily evaluate our increments. If for example we have [1, 1, 1, 1, 3, 5],
we don't need to process all the increments of duplicated 1s. We could take three ones (taken = [1, 1, 1])
and continue processing.
When we find an empty place like 2, 4, or 6, we can then recover that our increment will be 2-1, 4-1, and 6-1.

Algorithm

Count the values. For each possible value x:

If there are 2 or more values x in A, save the extra duplicated values to increment later.
If there are 0 values x in A, then a saved value v gets incremented to x.
In Java, the code is less verbose with a slight optimization: we record only the number of saved values,
and we subtract from the answer in advance. In the [1, 1, 1, 1, 3, 5] example, we do taken = 3 and ans -= 3 in advance,
and later we do ans += 2; ans += 4; ans += 6. This optimization is also used in Approach 2.

class Solution(object):
    def minIncrementForUnique(self, A):
        count = collections.Counter(A)
        taken = []
        ans = 0
        for x in xrange(100000):
            if count[x] >= 2:
                taken.extend([x] * (count[x] - 1))
            elif taken and count[x] == 0:
                ans += x - taken.pop()
        return ans
"""
from typing import List
from collections import Counter


class Solution:
    def minIncrementForUnique(self, A: List[int]) -> int:
        if 0 == len(A):
            return 0
        count = Counter(A)
        occupied_positions = set(count.keys())
        min_moves = 0
        for base_v, c in count.items():
            if 1 == c:
                continue
            curr_vals = [base_v] * (c - 1)
            possible_val = base_v + 1
            while True:
                if len(curr_vals) == 0:
                    break
                if possible_val not in occupied_positions:
                    min_moves += possible_val - curr_vals[-1]
                    occupied_positions.add(possible_val)
                    curr_vals.pop()
                possible_val += 1
        return min_moves


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [3,2,1,2,1,7]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.minIncrementForUnique(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))

