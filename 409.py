"""
Given a string which consists of lowercase or uppercase letters,
find the length of the longest palindromes that can be built with those letters.

This is case sensitive, for example "Aa" is not considered a palindrome here.

Note:
Assume the length of given string will not exceed 1,010.

Example:

Input:
"abccccdd"

Output:
7

Explanation:
One longest palindrome that can be built is "dccaccd", whose length is 7.
"""


class Solution:
    def longestPalindrome(self, s: str) -> int:
        from collections import Counter
        count = Counter(s)
        max_len = 0
        is_single_inside = False
        for item in count.items():
            if item[1] >= 2:
                if item[1] % 2 == 1:
                    is_single_inside = True
                max_len += item[1]//2 * 2
            elif 1 == item[1]:
                is_single_inside = True
        if is_single_inside:
            max_len += 1
        return max_len


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "abccccdd"
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.longestPalindrome(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
