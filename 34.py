# -*- coding: utf-8 -*-
"""
Given an array of integers nums sorted in ascending order,
find the starting and ending position of a given target value.

Your algorithm's runtime complexity must be in the order of O(log n).

If the target is not found in the array, return [-1, -1].

Example 1:

Input: nums = [5,7,7,8,8,10], target = 8
Output: [3,4]
Example 2:

Input: nums = [5,7,7,8,8,10], target = 6
Output: [-1,-1]
"""
from typing import List


class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        def _binary_search_left():
            low, high = 0, len(nums) - 1
            while low <= high:
                mid = (low + high) // 2
                if target == nums[mid]:
                    if 0 <= mid - 1 < len(nums) and target == nums[mid-1]:
                        high = mid - 1
                    else:
                        return mid
                elif target < nums[mid]:
                    high = mid - 1
                else:
                    low = mid + 1
            return -1

        #
        def _binary_search_right():
            low, high = 0, len(nums) - 1
            while low <= high:
                mid = (low + high) // 2
                if target == nums[mid]:
                    if 0 <= mid + 1 < len(nums) and target == nums[mid+1]:
                        low = mid + 1
                    else:
                        return mid
                elif target < nums[mid]:
                    high = mid - 1
                else:
                    low = mid + 1
            return -1
        #
        if len(nums) == 0:
            return [-1, -1]
        return [_binary_search_left(), _binary_search_right()]


if __name__ == "__main__":
    import timeit
    s = Solution()
    nums = [7, 7]
    target = 7
    s_t = timeit.default_timer()
    print(s.searchRange(nums, target))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
