"""
Given a non-empty array containing only positive integers,
find if the array can be partitioned into two subsets such that the sum of elements in both subsets is equal.
Note:
Each of the array element will not exceed 100.
The array size will not exceed 200.
------------------------------------------------
Example 1:
Input: [1, 5, 11, 5]
Output: true
Explanation: The array can be partitioned as [1, 5, 5] and [11].

Example 2:
Input: [1, 2, 3, 5]
Output: false
Explanation: The array cannot be partitioned into equal sum subsets.
"""


class Solution:
    def canPartition(self, nums) -> bool:
        if len(nums) < 2:
            return False
        total = sum(nums)
        if total % 2 == 1:
            return False
        #
        target_sum = total // 2
        max_length = len(nums) - 1
        # 构建图
        graph = {}
        for i in range(len(nums)):
            for j in range(len(nums)):
                if i not in graph:
                    graph[i] = []
                if i != j:
                    graph[i].append(j)
        # 深度优先搜索
        curr_sum = nums[0]
        stack = [0]
        curr_length = 1
        flag_is_passed = [False] * len(nums)
        flag_is_passed[0] = True
        if curr_sum == target_sum:
            return True
        while stack:
            curr_node = stack[-1]
            if curr_length > max_length or (not graph[curr_node]) \
                    or curr_sum > target_sum:
                curr_sum -= nums[curr_node]
                curr_length -= 1
                flag_is_passed[curr_node] = False
                _ = stack.pop()
                continue
            else:
                next_node = graph[curr_node].pop()
                if flag_is_passed[next_node]:
                    continue
                stack.append(next_node)
                curr_sum += nums[next_node]
                curr_length += 1
                flag_is_passed[next_node] = True
            if curr_sum == target_sum:
                return True
        return False


import timeit
s = Solution()
nums = [3,3,3,4,5]
# nums = [28,63,95,30,39,16,36,44,37,100,61,73,32,71,100,2,37,60,23,71,53,70,69,82,97,43,16,33,29,5,97,32,29,78,93,59,37,88,89,79,75,9,74,32,81,12,34,13,16,15,16,40,90,70,17,78,54,81,18,92,75,74,59,18,66,62,55,19,2,67,30,25,64,84,25,76,98,59,74,87,5,93,97,68,20,58,55,73,74,97,49,71,42,26,8,87,99,1,16,79]
print(len(nums))
s_t = timeit.default_timer()
print(s.canPartition(nums))
e_t = timeit.default_timer()
print("time cost: {:.5f} seconds.".format(e_t - s_t))

