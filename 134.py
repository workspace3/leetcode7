# -*- coding: utf-8 -*-
"""
There are N gas stations along a circular route, where the amount of gas at station i is gas[i].

You have a car with an unlimited gas tank and
it costs cost[i] of gas to travel from station i to its next station (i+1).
You begin the journey with an empty tank at one of the gas stations.

Return the starting gas station's index if you can travel around the circuit once in the clockwise direction,
otherwise return -1.

Note:

If there exists a solution, it is guaranteed to be unique.
Both input arrays are non-empty and have the same length.
Each element in the input arrays is a non-negative integer.
Example 1:

Input:
gas  = [1,2,3,4,5]
cost = [3,4,5,1,2]

Output: 3

Explanation:
Start at station 3 (index 3) and fill up with 4 unit of gas. Your tank = 0 + 4 = 4
Travel to station 4. Your tank = 4 - 1 + 5 = 8
Travel to station 0. Your tank = 8 - 2 + 1 = 7
Travel to station 1. Your tank = 7 - 3 + 2 = 6
Travel to station 2. Your tank = 6 - 4 + 3 = 5
Travel to station 3. The cost is 5. Your gas is just enough to travel back to station 3.
Therefore, return 3 as the starting index.
Example 2:

Input:
gas  = [2,3,4]
cost = [3,4,3]

Output: -1

Explanation:
You can't start at station 0 or 1, as there is not enough gas to travel to the next station.
Let's start at station 2 and fill up with 4 unit of gas. Your tank = 0 + 4 = 4
Travel to station 0. Your tank = 4 - 3 + 2 = 3
Travel to station 1. Your tank = 3 - 3 + 3 = 3
You cannot travel back to station 2, as it requires 4 unit of gas but you only have 3.
Therefore, you can't travel around the circuit once no matter where you start.
--------其他解法---------
I saw lots of posts are using if sum(gas) < sum(cost): return -1 to filter out 'No-Solution' test cases,
but in interview, if you can't remember how to proof it, then maybe better not to use it

Here's another solution that does not use that trick but still beats 100%.

The idea is based on the observation that:

If proceed from A, and found A cannot reach B, then for any points C between A and B; C cannot reach B too.
(because if A reached C, then the fuel left when reached C will always >= 0,
which is always equal or better than start from C)

same idea, if A can reach B, then for any points C between A and B; C can reach B too.

So the algorithm is:

start from index start (initialized as 0) and proceeds, record the fuel left in tank.

if we are lucky and sucessfully returned to point start, than we return index 'start' :)

however, if at index i, we found that we can't proceed to i + 1,
then we record how many gas are we lacking (stored in gap),
and re-start from i + 1, and we update the start index.

keep runing untill we returned to the 0 point.
now the variable gap stores the information that
how many fuel we need in order to start from 0 and returned sucessfully reach to index start
(which records the last start position),
and we also have a tank variable
that tells us how many fuel we left when we start from start and reach 0, if tank >= gap,
then we can return to the start point, if not, we can't.

Here's the code

class Solution(object):
    def canCompleteCircuit(self, gas, cost):
        tank = gap = start = 0
        for i in range(len(gas)):
            tank += gas[i]
            if tank >= cost[i]:
                tank -= cost[i]
            else:
                gap += cost[i] - tank
                start = i + 1
                tank = 0
        if start == len(gas) or tank < gap: return -1
        return start
"""
from typing import List


class Solution:
    def canCompleteCircuit(self, gas: List[int], cost: List[int]) -> int:
        """
        思路：1）从index=0出发，记录tank里的油。
              2）当走到某个加油站i，不能从i到i+1时，此时，tank里的油为负数，即为如果要从0连续走到i+1，缺失的油量。
              3）那么此时，记录此缺失油量，然后起点更新为i+1，重新出发，tank复位为0，重新记录tank里的油。
              重复2）和3）直到最后一个加油站len(gas) - 1。
              此时tank里的剩余油量如果刚好为负数，那么说明没有符合条件的起点；
              如果不是，那么计算剩余油量是否可以cover掉之前一直记录的缺失的油量，
              如果可以，那么当前起点即为可用起点，否则说明没有符合条件的起点。
              #
              概括：从index=0开始，到index=len(gas) - 1，一段一段的走，
                    记录每一段的总的油量（中间段都是负数，即连接下一段的缺失油量），
                    然后看走到最后剩余的油量是否能够将段与段之间连接起来。
        :param gas:
        :param cost:
        :return:
        """
        tank = 0
        lack = 0
        start_gas_index = 0
        for i in range(len(gas)):
            tank += (gas[i] - cost[i])
            if tank < 0:
                lack += tank
                tank = 0
                start_gas_index = i + 1
        if tank < 0:
            return -1
        else:
            if tank + lack >= 0:
                return start_gas_index
            else:
                return -1


if __name__ == "__main__":
    import timeit
    s = Solution()
    gas = [1, 2, 3, 4, 5]
    cost = [3, 4, 5, 1, 2]
    s_t = timeit.default_timer()
    print(s.canCompleteCircuit(gas, cost))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
