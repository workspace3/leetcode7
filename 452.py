# -*- coding: utf-8 -*-
"""
There are a number of spherical balloons spread in two-dimensional space.
For each balloon, provided input is the start and end coordinates of the horizontal diameter.
Since it's horizontal, y-coordinates don't matter and hence the x-coordinates of start and end of the diameter suffice.
Start is always smaller than end. There will be at most 104 balloons.

An arrow can be shot up exactly vertically from different points along the x-axis.
A balloon with xstart and xend bursts by an arrow shot at x if xstart ≤ x ≤ xend.
There is no limit to the number of arrows that can be shot. An arrow once shot keeps travelling up infinitely.
The problem is to find the minimum number of arrows that must be shot to burst all balloons.

Example:

Input:
[[10,16], [2,8], [1,6], [7,12]]

Output:
2

Explanation:
One way is to shoot one arrow for example at x = 6 (bursting the balloons [2,8] and [1,6])
and another arrow at x = 11 (bursting the other two balloons).
"""
from typing import List


class Solution:
    def findMinArrowShots(self, points: List[List[int]]) -> int:
        length = len(points)
        if 0 == length:
            return 0
        points = sorted(points)
        result = [points[0][1]]
        for i in range(1, length):
            if points[i][0] > result[-1]:
                result.append(points[i][1])
            else:
                pop_point = result.pop()
                result.append(min(points[i][1], pop_point))
        return len(result)


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [[10,16], [2,8], [1,6], [7,12]]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.findMinArrowShots(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
