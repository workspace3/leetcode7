from typing import List


class Solution:
    def reverseVowels(self, s: str) -> str:
        if len(s) == 0:
            return s
        s = list(s)
        vowels = {'a', 'i', 'o', 'e', 'u'}
        left, right = 0, len(s) - 1
        while left < right:
            if s[left] in vowels and s[right] in vowels:
                tmp = s[left]
                s[left] = s[right]
                s[right] = tmp
            elif s[left] in vowels and s[right] not in vowels:
                right -= 1
            elif s[left] not in vowels and s[right] in vowels:
                left += 1
            else:
                right -= 1
                left += 1
        return ''.join(s)


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "asdsdsdsd"
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.reverseVowels(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))

