"""
Given a string, find the length of the longest substring without repeating characters.

Example 1:

Input: "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.
Example 2:

Input: "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
Example 3:

Input: "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
             Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
"""


class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        """
        reference to the answer of shaoyu0966
        """
        if not s:
            return 0
        length = len(s)
        max_len = 0
        record = {}
        curr_start_index = 0
        curr_sub_length = 0
        for i in range(length):
            if s[i] not in record or record[s[i]] < curr_start_index:
                curr_sub_length += 1
            else:
                max_len = max(max_len, curr_sub_length)
                curr_start_index = record[s[i]] + 1
                curr_sub_length = i - curr_start_index + 1
            record[s[i]] = i
        return max(max_len, curr_sub_length)

    def lengthOfLongestSubstring_from_others(self, s: str) -> int:
        str_len = len(s)
        last_occur = dict()
        max_len = 0
        cur_len = 0
        cur_start = 0
        if str_len == 0:
            return 0

        for i in range(str_len):
            c = s[i]
            if c not in last_occur or last_occur[c] < cur_start:
                last_occur[c] = i
                cur_len = cur_len + 1
            else:
                max_len = max(max_len, cur_len)
                cur_start = last_occur[c] + 1
                cur_len = i - cur_start + 1
                last_occur[c] = i
        return max(max_len, cur_len)


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "aui"
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.lengthOfLongestSubstring(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))