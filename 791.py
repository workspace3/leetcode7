"""
S and T are strings composed of lowercase letters. In S, no letter occurs more than once.

S was sorted in some custom order previously.
We want to permute the characters of T so that they match the order that S was sorted.
More specifically, if x occurs before y in S, then x should occur before y in the returned string.

Return any permutation of T (as a string) that satisfies this property.

Example :
Input:
S = "cba"
T = "abcd"
Output: "cbad"
Explanation:
"a", "b", "c" appear in S, so the order of "a", "b", "c" should be "c", "b", and "a".
Since "d" does not appear in S, it can be at any position in T. "dcba", "cdba", "cbda" are also valid outputs.
"""


class Solution:
    def customSortString(self, S: str, T: str) -> str:
        from collections import Counter
        s = dict([(v, k) for k, v in enumerate(S)])
        t = sorted([(k, v, s[k]) if k in s else (k, v, -1) for k, v in Counter(T).items()], key=lambda x: x[-1])
        return "".join([k*v for k, v, _ in t])


if __name__ == "__main__":
    import timeit
    s = Solution()
    S = "cbaepk"
    T = "abkjkeifnksdsosbbpkegcd"
    s_t = timeit.default_timer()
    print(s.customSortString(S, T))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))