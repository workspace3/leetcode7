"""
Write a program to find the nth super ugly number.

Super ugly numbers are positive numbers whose all prime factors are in the given prime list primes of size k.

Example:

Input: n = 12, primes = [2,7,13,19]
Output: 32
Explanation: [1,2,4,7,8,13,14,16,19,26,28,32] is the sequence of the first 12
             super ugly numbers given primes = [2,7,13,19] of size 4.
Note:

1 is a super ugly number for any given primes.
The given numbers in primes are in ascending order.
0 < k ≤ 100, 0 < n ≤ 106, 0 < primes[i] < 1000.
The nth super ugly number is guaranteed to fit in a 32-bit signed integer.
"""

class Solution:
    def nthSuperUglyNumber(self, n: int, primes) -> int:
        length = len(primes)
        pointer = [0]*length
        result = [1]
        curr_len = 1
        curr_result = [primes[k]*result[pointer[k]] for k in range(length)]
        changed_point = -1
        while curr_len < n:
            if -1 != changed_point:
                curr_result[changed_point] = primes[changed_point]*result[pointer[changed_point]]
            min_val = min(curr_result)
            if min_val != result[-1]:
                result.append(min_val)
                curr_len += 1
            min_index = curr_result.index(min_val)
            pointer[min_index] += 1
            changed_point = min_index
        return result[-1]


if __name__ == "__main__":
    import timeit
    s = Solution()
    n = 12
    primes = [2,7,13,19]
    s_t = timeit.default_timer()
    print(s.nthSuperUglyNumber(n, primes))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))