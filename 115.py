"""
Given a string S and a string T, count the number of distinct subsequences of S which equals T.

A subsequence of a string is a new string which is formed from the original string by
deleting some (can be none) of the characters without disturbing the relative positions of the remaining characters.
(ie, "ACE" is a subsequence of "ABCDE" while "AEC" is not).

Example 1:

Input: S = "rabbbit", T = "rabbit"
Output: 3
Explanation:

As shown below, there are 3 ways you can generate "rabbit" from S.
(The caret symbol ^ means the chosen letters)

rabbbit
^^^^ ^^
rabbbit
^^ ^^^^
rabbbit
^^^ ^^^
Example 2:

Input: S = "babgbag", T = "bag"
Output: 5
Explanation:

As shown below, there are 5 ways you can generate "bag" from S.
(The caret symbol ^ means the chosen letters)

babgbag
^^ ^
babgbag
^^    ^
babgbag
^    ^^
babgbag
  ^  ^^
babgbag
    ^^^
"""


class Solution:
    def numDistinct(self, s: str, t: str) -> int:
        if len(t) == 0:
            return 0
        elif len(t) == 1:
            return s.count(t)
        else:
            nums = 0
            for i in range(len(s)):
                if t[0] == s[i]:
                    new_s = "" if i+1 >= len(s) else s[i+1:]
                    nums += self.numDistinct(new_s, t[1:])
            return nums

    def numDistinct_from_others(self, s: str, t: str) -> int:
        # s is the long -> use i
        # t is the short -> use j

        dp = [0 for i in range(len(t) + 1)]
        dp[0] = 1  # 1 way to get empty substring

        for i in range(0, len(s)):
            for j in range(len(t), 0, -1):  # reverse to avoid stepping over the current iteration
                if s[i] == t[j - 1]:
                    dp[j] += dp[j - 1]

        return dp[-1]


import timeit
s = Solution()
S = "daacaedaceacabbaabdccdaaeaebacddadcaeaacadbceaecddecdeedcebcdacdaebccdeebcbdeaccabcecbeeaadbccbaeccbbdaeadecabbbedceaddcdeabbcdaeadcddedddcececbeeabcbecaeadddeddccbdbcdcbceabcacddbbcedebbcaccac"
T = "ceadbaa"
s_t = timeit.default_timer()
print(s.numDistinct_from_others(S, T))
e_t = timeit.default_timer()
print("time cost: {:.5f} seconds.".format(e_t - s_t))
