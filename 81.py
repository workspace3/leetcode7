# -*- coding: utf-8 -*-
"""
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e., [0,0,1,2,2,5,6] might become [2,5,6,0,0,1,2]).

You are given a target value to search. If found in the array return true, otherwise return false.

Example 1:

Input: nums = [2,5,6,0,0,1,2], target = 0
Output: true

Example 2:

Input: nums = [2,5,6,0,0,1,2], target = 3
Output: false

Follow up:

    This is a follow up problem to Search in Rotated Sorted Array, where nums may contain duplicates.
    Would this affect the run-time complexity? How and why?


"""
from typing import List


class Solution:
    def search(self, nums: List[int], target: int) -> bool:
        return target in set(nums)


if __name__ == "__main__":
    import timeit
    s = Solution()
    nums = [2,5,6,0,0,1,2]
    target = 3
    print(len(nums))
    s_t = timeit.default_timer()
    print(s.search(nums, target))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
