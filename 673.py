"""
Given an unsorted array of integers, find the number of longest increasing subsequence.

Example 1:
Input: [1,3,5,4,7]
Output: 2
Explanation: The two longest increasing subsequence are [1, 3, 4, 7] and [1, 3, 5, 7].
Example 2:
Input: [2,2,2,2,2]
Output: 5
Explanation: The length of longest continuous increasing subsequence is 1,
and there are 5 subsequences' length is 1, so output 5.
Note: Length of the given array will be not exceed 2000 and the answer is guaranteed to be fit in 32-bit signed int.
"""


class Solution:
    def findNumberOfLIS(self, nums) -> int:
        # dp[i]表示以nums[i]结尾的最长子串的长度
        length = len(nums)
        if 0 == length:
            return 0
        dp = [1]*length
        count = [1]*length
        for i in range(1, length):
            for j in range(i)[-1::-1]:
                if nums[j] < nums[i]:
                    if dp[i] == dp[j] + 1:
                        count[i] += count[j]
                    elif dp[i] < dp[j] + 1:
                        dp[i] = dp[j] + 1
                        count[i] = count[j]

        #
        result = sorted(zip(dp, count), key=lambda x: x[0])
        ct = 0
        max_len = result[-1][0]
        for item in result[-1::-1]:
            if item[0] == max_len:
                ct += item[1]
        return ct


if __name__ == "__main__":
    import timeit
    s = Solution()
    # nums = [2,4,3,10,3,2,4,4,9,3,5,8,8,9,3,0,4,3,3,9,4,3,4,1,9,7,2,9,1,9,10,5,5,5,5,3,3,10,9,3,7,6,6,2,7,7,9,8,3,7,2,4,4,9,4,5,10,7,-1,0,5,1,9,4,2,3,0,5,0,2,8,1,0,7,10,4,8,3,6,0,4,3,3,8,4]
    nums = [1,3,5,4,7]
    print(len(nums))
    s_t = timeit.default_timer()
    print(s.findNumberOfLIS(nums))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))