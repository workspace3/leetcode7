"""
The gray code is a binary numeral system where two successive values differ in only one bit.

Given a non-negative integer n representing the total number of bits in the code,
print the sequence of gray code. A gray code sequence must begin with 0.

Example 1:

Input: 2
Output: [0,1,3,2]
Explanation:
00 - 0
01 - 1
11 - 3
10 - 2

For a given n, a gray code sequence may not be uniquely defined.
For example, [0,2,3,1] is also a valid gray code sequence.

00 - 0
10 - 2
11 - 3
01 - 1
Example 2:

Input: 0
Output: [0]
Explanation: We define the gray code sequence to begin with 0.
             A gray code sequence of n has size = 2n, which for n = 0 the size is 20 = 1.
             Therefore, for n = 0 the gray code sequence is [0].
--------其他解法---------
https://blog.csdn.net/w8253497062015/article/details/80896500
"""
from typing import List


class Solution:
    def grayCode(self, n: int) -> List[int]:
        # todo 参考格雷码的生成方式
        return List[int]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = 3
    s_t = timeit.default_timer()
    print(s.grayCode(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))



