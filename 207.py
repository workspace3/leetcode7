# -*- coding: utf-8 -*-
"""
There are a total of numCourses courses you have to take, labeled from 0 to numCourses-1.

Some courses may have prerequisites, for example to take course 0 you have to first take course 1,
which is expressed as a pair: [0,1]

Given the total number of courses and a list of prerequisite pairs, is it possible for you to finish all courses?



Example 1:

Input: numCourses = 2, prerequisites = [[1,0]]
Output: true
Explanation: There are a total of 2 courses to take.
             To take course 1 you should have finished course 0. So it is possible.
Example 2:

Input: numCourses = 2, prerequisites = [[1,0],[0,1]]
Output: false
Explanation: There are a total of 2 courses to take.
             To take course 1 you should have finished course 0, and to take course 0 you should
             also have finished course 1. So it is impossible.


Constraints:

The input prerequisites is a graph represented by a list of edges, not adjacency matrices.
Read more about how a graph is represented.
You may assume that there are no duplicate edges in the input prerequisites.
1 <= numCourses <= 10^5
"""
from typing import List


class Solution:
    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        # calculate in-degree and out-nodes
        # find all nodes where in-degree is 0
        #
        if len(prerequisites) == 0:
            return True
        records = dict()
        for edge in prerequisites:
            child, parent = edge[0], edge[1]
            if parent not in records:
                records[parent] = {"in": [], "out": [], "in_degree": 0}
            if child not in records:
                records[child] = {"in": [], "out": [], "in_degree": 0}
            records[parent]["out"].append(child)
            records[child]["in"].append(parent)
        #
        zero_in_degree_nodes = []
        for node in records:
            records[node]["in_degree"] = len(set(records[node]["in"]))
            del records[node]["in"]
            if 0 == records[node]["in_degree"]:
                zero_in_degree_nodes.append(node)
        # 有环
        if len(zero_in_degree_nodes) == 0:
            return False
        #
        while zero_in_degree_nodes:
            curr_node = zero_in_degree_nodes.pop(-1)
            child_nodes = records[curr_node]["out"]
            del records[curr_node]
            for child in child_nodes:
                records[child]["in_degree"] -= 1
                if 0 == records[child]["in_degree"]:
                    zero_in_degree_nodes.append(child)
        if len(records) > 0:
            return False
        else:
            return True


if __name__ == "__main__":
    import timeit
    s = Solution()
    numCourses = 2
    prerequisites = [[1,0],[0,1]]
    s_t = timeit.default_timer()
    print(s.canFinish(numCourses, prerequisites))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))

