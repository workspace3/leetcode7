"""
We have some permutation A of [0, 1, ..., N - 1], where N is the length of A.

The number of (global) inversions is the number of i < j with 0 <= i < j < N and A[i] > A[j].

The number of local inversions is the number of i with 0 <= i < N and A[i] > A[i+1].

Return true if and only if the number of global inversions is equal to the number of local inversions.

Example 1:

Input: A = [1,0,2]
Output: true
Explanation: There is 1 global inversion, and 1 local inversion.
Example 2:

Input: A = [1,2,0]
Output: false
Explanation: There are 2 global inversions, and 1 local inversion.
Note:

A will be a permutation of [0, 1, ..., A.length - 1].
A will have length in range [1, 5000].
The time limit for this problem has been reduced.
--------其他解法----------
https://www.jianshu.com/p/e717e24c5796
"""
from typing import List


class Solution:
    def isIdealPermutation(self, A: List[int]) -> bool:
        if 1 == len(A):
            return True
        global_count = 0
        local_count = 0
        for i in range(1, len(A)):
            for j in range(i):
                if A[j] > A[i]:
                    if j == i - 1:
                        local_count += 1
                    global_count += 1
                    if global_count > local_count:
                        return False
        return True


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [1, 0, 2]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.isIdealPermutation(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
