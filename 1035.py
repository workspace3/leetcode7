"""
We write the integers of A and B (in the order they are given) on two separate horizontal lines.

Now, we may draw connecting lines: a straight line connecting two numbers A[i] and B[j] such that:

A[i] == B[j];
The line we draw does not intersect any other connecting (non-horizontal) line.
Note that a connecting lines cannot intersect even at the endpoints: each number can only belong to one connecting line.

Return the maximum number of connecting lines we can draw in this way.



Example 1:


Input: A = [1,4,2], B = [1,2,4]
Output: 2
Explanation: We can draw 2 uncrossed lines as in the diagram.
We cannot draw 3 uncrossed lines, because the line from A[1]=4 to B[2]=4 will intersect the line from A[2]=2 to B[1]=2.
Example 2:

Input: A = [2,5,1,2,5], B = [10,5,2,1,5,2]
Output: 3
Example 3:

Input: A = [1,3,7,1,7,5], B = [1,9,2,5,1]
Output: 2


Note:

1 <= A.length <= 500
1 <= B.length <= 500
1 <= A[i], B[i] <= 2000

--------------其他人解法--------------
This is the same with finding the length of the longest common subsequence of A and B.
Therefore, we can solve it by dynamic programming.

class Solution:
    def maxUncrossedLines(self, A: List[int], B: List[int]) -> int:
        dp = [[0] * (len(B) + 1) for _ in range(len(A) + 1)]
        for i in range(len(A)):
            for j in range(len(B)):
                dp[i + 1][j + 1] = max(dp[i][j + 1], dp[i + 1][j], dp[i][j] + (A[i] == B[j]))
        return dp[-1][-1]
"""
from typing import List


class Solution:
    def maxUncrossedLines(self, A: List[int], B: List[int]) -> int:
        if len(B) == 0:
            return 0
        elif len(A) == 0:
            return 0
        else:
            indexes = []
            for b_index in range(len(B)):
                if B[b_index] == A[0]:
                    indexes.append(b_index)
            if len(indexes) == 0:
                return self.maxUncrossedLines(A[1:], B)
            else:
                return max([self.maxUncrossedLines(A[1:], B)] +
                           [1+self.maxUncrossedLines(A[1:], B[i+1:]) for i in indexes])


if __name__ == "__main__":
    import timeit
    s = Solution()
    A = [2,5,1,2,5]
    B = [10,5,2,1,5,2]
    s_t = timeit.default_timer()
    print(s.maxUncrossedLines(A, B))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
