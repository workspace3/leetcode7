# -*- coding: utf-8 -*-
"""
You are given coins of different denominations and a total amount of money amount.
Write a function to compute the fewest number of coins that you need to make up that amount.
If that amount of money cannot be made up by any combination of the coins, return -1.

Example 1:

Input: coins = [1, 2, 5], amount = 11
Output: 3
Explanation: 11 = 5 + 5 + 1
Example 2:

Input: coins = [2], amount = 3
Output: -1
Note:
You may assume that you have an infinite number of each kind of coin.
------------------

"""
from typing import List


class Solution:
    def coinChange(self, coins: List[int], amount: int) -> int:
        dp = [amount+1]*(amount+1)
        dp[0] = 0
        for i in range(amount+1):
            candidates = [dp[i-coins[j]]+1 for j in range(len(coins)) if i >= coins[j]]
            if len(candidates) > 0:
                dp[i] = min(candidates)
        return dp[amount] if dp[amount] <= amount else -1


if __name__ == "__main__":
    import timeit
    s = Solution()
    coins = [1, 2, 5, 10]
    amount = 27
    s_t = timeit.default_timer()
    print(s.coinChange(coins, amount))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
