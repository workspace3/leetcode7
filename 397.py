# -*- coding: utf-8 -*-
"""
Given a positive integer n and you can do operations as follow:

If n is even, replace n with n/2.
If n is odd, you can replace n with either n + 1 or n - 1.
What is the minimum number of replacements needed for n to become 1?

Example 1:

Input:
8

Output:
3

Explanation:
8 -> 4 -> 2 -> 1
Example 2:

Input:
7

Output:
4

Explanation:
7 -> 8 -> 4 -> 2 -> 1
or
7 -> 6 -> 3 -> 2 -> 1
"""


class Solution:
    def integerReplacement(self, n: int) -> int:
        # 递归
        if 1 >= n:
            return 0
        elif n % 2 == 0:
            return 1 + self.integerReplacement(n // 2)
        else:
            return 1 + min(self.integerReplacement(n + 1),
                           self.integerReplacement(n - 1))


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = 8
    s_t = timeit.default_timer()
    print(s.integerReplacement(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
