# -*- coding: utf-8 -*-

"""
英文版leetcode算法篇，第826号问题
问题描述：
826. Most Profit Assigning Work
We have jobs: difficulty[i] is the difficulty of the ith job, and profit[i] is the profit of the ith job.
Now we have some workers. worker[i] is the ability of the ith worker, which means that this worker can only
complete a job with difficulty at most worker[i].
Every worker can be assigned at most one job, but one job can be completed multiple times.
For example, if 3 people attempt the same job that pays $1, then the total profit will be $3.
If a worker cannot complete any job, his profit is $0.
What is the most profit we can make?
--------------------------------------------------
Example 1:
Input: difficulty = [2,4,6,8,10], profit = [10,20,30,40,50], worker = [4,5,6,7]
Output: 100
Explanation: Workers are assigned jobs of difficulty [4,4,6,6] and they get profit of [20,20,30,30] seperately.
--------------------------------------------------

Notes:
1 <= difficulty.length = profit.length <= 10000
1 <= worker.length <= 10000
difficulty[i], profit[i], worker[i]  are in range [1, 10^5]
"""


class Solution:
    def maxProfitAssignment(self, difficulty, profit, worker):
        #
        sorted_data = sorted(zip(difficulty, profit), key=lambda x: x[0])
        worker = sorted(worker)
        max_profits = [0]
        start_index = 0
        for curr_capacity in worker:
            if curr_capacity < sorted_data[0][0]:
                max_profits.append(0)
            curr_max_profit = max_profits[-1]
            is_break = False
            for i in range(start_index, len(sorted_data)):
                if sorted_data[i][0] <= curr_capacity:
                    if curr_max_profit < sorted_data[i][1]:
                        curr_max_profit = sorted_data[i][1]
                else:
                    start_index = i
                    is_break = True
                    break
            if not is_break:
                start_index = len(sorted_data)
            max_profits.append(curr_max_profit)
        print(max_profits)
        return sum(max_profits)

    def maxProfitAssignment_from_other_answers(self, difficulty, profit, worker):
        jobs = zip(difficulty, profit)
        jobs = sorted(jobs, key=lambda x: x[0])
        max_profit = float('-inf')
        new_jobs = []
        for job in jobs:
            max_profit = max(max_profit, job[1])
            new_jobs.append((job[0], max_profit))
        del jobs
        # print(new_jobs)
        # print(sorted(worker))

        length = len(new_jobs)
        total_pay = 0
        idx = 0
        last_profit = None
        for w in sorted(worker):
            if idx == length:
                if last_profit is not None:
                    total_pay += last_profit
            while idx < length:
                if w >= new_jobs[idx][0]:
                    if idx == length - 1:
                        total_pay += new_jobs[idx][1]
                    idx += 1
                    last_profit = new_jobs[idx - 1][1]
                else:
                    if last_profit is not None:
                        total_pay += last_profit
                    break
                    # print(last_profit)

        return total_pay


difficulty = [13,37,58]
profit = [4,90,96]
worker = [34,73,45]
s = Solution()
import timeit
s_t = timeit.default_timer()
print(s.maxProfitAssignment(difficulty, profit, worker))
e_t = timeit.default_timer()
print("time cost: {:.5f} seconds.".format(e_t - s_t))






