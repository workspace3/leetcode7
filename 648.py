# -*- coding: utf-8 -*-
"""
In English, we have a concept called root,
which can be followed by some other words to form another longer word -
let's call this word successor.
For example, the root an, followed by other, which can form another word another.

Now, given a dictionary consisting of many roots and a sentence.
You need to replace all the successor in the sentence with the root forming it.
If a successor has many roots can form it, replace it with the root with the shortest length.

You need to output the sentence after the replacement.

Example 1:

Input: dict = ["cat", "bat", "rat"]
sentence = "the cattle was rattled by the battery"
Output: "the cat was rat by the bat"


Note:

The input will only have lower-case letters.
1 <= dict words number <= 1000
1 <= sentence words number <= 1000
1 <= root length <= 100
1 <= sentence words length <= 1000
"""
from typing import List


class Solution:
    def replaceWords(self, dict: List[str], sentence: str) -> str:
        reference = {}
        for item in dict:
            length = len(item)
            start_character = item[0]
            if length not in reference:
                reference[length] = {}
            if start_character not in reference[length]:
                reference[length][start_character] = []
            reference[length][start_character].append(item)
        #
        sent = []
        for word in sentence.split():
            word_len = len(word)
            candidates = sorted(filter(lambda x: x <= word_len, reference.keys()))
            # find a root
            is_found = False
            for curr_len in candidates:
                start_character = word[0]
                if start_character in reference[curr_len]:
                    for root in reference[curr_len][start_character]:
                        if word.startswith(root):
                            sent.append(root)
                            is_found = True
                            break
                if is_found:
                    break
            #
            if not is_found:
                sent.append(word)
        return " ".join(sent)


if __name__ == "__main__":
    import timeit
    s = Solution()
    _dict = ["cat", "bat", "rat"]
    sentence = "the cattle was rattled by the battery"
    s_t = timeit.default_timer()
    print(s.replaceWords(_dict, sentence))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
