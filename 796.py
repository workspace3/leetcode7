"""
We are given two strings, A and B.

A shift on A consists of taking string A and moving the leftmost character to the rightmost position.
For example, if A = 'abcde', then it will be 'bcdea' after one shift on A.
Return True if and only if A can become B after some number of shifts on A.

Example 1:
Input: A = 'abcde', B = 'cdeab'
Output: true

Example 2:
Input: A = 'abcde', B = 'abced'
Output: false
Note:

A and B will have length at most 100.
"""
import re


class Solution:
    def rotateString(self, A: str, B: str) -> bool:
        def find_all(c, S):
            return [item.start() for item in re.finditer(c, S)]
        #
        if len(A) != len(B):
            return False
        #
        if not A:
            return True
        #
        indexes = find_all(A[0], B)
        if not indexes:
            return False
        for i in indexes:
            new_B = B[i:] + B[:i]
            if A == new_B:
                return True
        return False


if __name__ == "__main__":
    import timeit
    s = Solution()
    A = ''
    B = ''
    print((len(A), len(B)))
    s_t = timeit.default_timer()
    print(s.rotateString(A, B))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))