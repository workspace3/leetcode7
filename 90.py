# -*- coding: utf-8 -*-
"""
Given a collection of integers that might contain duplicates, nums,
return all possible subsets (the power set).

Note: The solution set must not contain duplicate subsets.

Example:

Input: [1,2,2]
Output:
[
  [2],
  [1],
  [1,2,2],
  [2,2],
  [1,2],
  []
]
"""


class Solution:
    def subsetsWithDup(self, nums):
        from collections import Counter
        count = Counter(nums)
        result = [[]]
        for k, v in count.items():
            curr_result = result[0:]
            add_result = []
            for i in range(v):
                add_item = [k] * (i+1)
                for item in curr_result:
                    add_result.append(item+add_item)
            result.extend(add_result)
        return result


import timeit
s = Solution()
nums = [1,2,2]
print(len(nums))
s_t = timeit.default_timer()
print(s.subsetsWithDup(nums))
e_t = timeit.default_timer()
print("time cost: {:.5f} seconds.".format(e_t - s_t))