# -*- coding: utf-8 -*-
"""
Given a string s, find the longest palindromic substring in s.
You may assume that the maximum length of s is 1000.

Example 1:

Input: "babad"
Output: "bab"
Note: "aba" is also a valid answer.
Example 2:

Input: "cbbd"
Output: "bb"
"""


class Solution:
    def longestPalindrome(self, s: str) -> str:
        if len(s) == 0:
            return ""
        result = (s[0], 1)
        for i in range(len(s)-1):
            curr_max_delta = 0
            for j in range(1, len(s)*2):
                if not (0 <= i-j < len(s) and 0 <= i+j < len(s)):
                    break
                left, right = s[i-j], s[i+j]
                if left == right:
                    curr_max_delta += 1
                else:
                    break
            curr_max = 1 + 2*curr_max_delta
            if curr_max > result[-1]:
                result = (s[i-curr_max_delta:i+curr_max_delta+1], curr_max)
            #
            if s[i] != s[i+1]:
                continue
            curr_max_delta = 0
            for j in range(1, len(s)*2):
                if not (0 <= i-j < len(s) and 0 <= i+j+1 < len(s)):
                    break
                left, right = s[i-j], s[i+1+j]
                if left == right:
                    curr_max_delta += 1
                else:
                    break
            curr_max = 2 + 2*curr_max_delta
            if curr_max > result[-1]:
                result = (s[i-curr_max_delta:i+curr_max_delta+2], curr_max)
        return result[0]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "a"
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.longestPalindrome(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
