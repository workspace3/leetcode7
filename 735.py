"""
We are given an array asteroids of integers representing asteroids in a row.

For each asteroid, the absolute value represents its size,
and the sign represents its direction (positive meaning right,
negative meaning left). Each asteroid moves at the same speed.

Find out the state of the asteroids after all collisions.
If two asteroids meet, the smaller one will explode.
If both are the same size, both will explode. Two asteroids moving in the same direction will never meet.

Example 1:
Input:
asteroids = [5, 10, -5]
Output: [5, 10]
Explanation:
The 10 and -5 collide resulting in 10.  The 5 and 10 never collide.
Example 2:
Input:
asteroids = [8, -8]
Output: []
Explanation:
The 8 and -8 collide exploding each other.
Example 3:
Input:
asteroids = [10, 2, -5]
Output: [10]
Explanation:
The 2 and -5 collide resulting in -5.  The 10 and -5 collide resulting in 10.
Example 4:
Input:
asteroids = [-2, -1, 1, 2]
Output: [-2, -1, 1, 2]
Explanation:
The -2 and -1 are moving left, while the 1 and 2 are moving right.
Asteroids moving the same direction never meet, so no asteroids will meet each other.
Note:

The length of asteroids will be at most 10000.
Each asteroid will be a non-zero integer in the range [-1000, 1000]..
"""
from typing import List


class Solution:
    def asteroidCollision(self, asteroids: List[int]) -> List[int]:
        dp = [[asteroids[i]] for i in range(len(asteroids))]
        for i in range(1, len(asteroids)):
            # 不碰撞
            if len(dp[i-1]) == 0 or not(asteroids[i] < 0 and dp[i-1][-1] > 0):
                dp[i] = dp[i-1]+[asteroids[i]]
                continue
            # 碰撞
            is_break = False
            for j in range(len(dp[i-1])-1, -1, -1):
                if not (asteroids[i] < 0 and dp[i-1][j] > 0):
                    dp[i] = dp[i - 1][:j + 1] + [asteroids[i]]
                    is_break = True
                    break
                if abs(dp[i-1][j]) < abs(asteroids[i]):
                    continue
                elif abs(dp[i-1][j]) > abs(asteroids[i]):
                    dp[i] = dp[i-1][:j+1]
                    is_break = True
                    break
                else:
                    dp[i] = dp[i-1][:j]
                    is_break = True
                    break
            if not is_break:
                dp[i] = [asteroids[i]]
        return dp[-1]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [1,-1,-2,-2]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.asteroidCollision(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))