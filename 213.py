"""
You are a professional robber planning to rob houses along a street.
Each house has a certain amount of money stashed.
All houses at this place are arranged in a circle. That means the first house is the neighbor of the last one.
Meanwhile, adjacent houses have security system connected
and it will automatically contact the police if two adjacent houses were broken into on the same night.

Given a list of non-negative integers representing the amount of money of each house,
determine the maximum amount of money you can rob tonight without alerting the police.

Example 1:

Input: [2,3,2]
Output: 3
Explanation: You cannot rob house 1 (money = 2) and then rob house 3 (money = 2),
             because they are adjacent houses.
Example 2:

Input: [1,2,3,1]
Output: 4
Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
             Total amount you can rob = 1 + 3 = 4.
"""


class Solution:
    def rob(self, nums) -> int:
        def sub_search(sub_nums):
            if not sub_nums:
                return 0
            dp = {0: sub_nums[0],
                  1: max(sub_nums[0:2])}
            length = len(sub_nums)
            if length < 3:
                return dp[length-1]
            for i in range(2, len(sub_nums)):
                dp[i] = max(dp[i-1], dp[i-2]+sub_nums[i])
            return dp[length-1]

        #
        if len(nums) < 3:
            return sub_search(nums)
        first_search = sub_search(nums[1:])
        second_search = sub_search(nums[0:-1])
        return max(first_search, second_search)


if __name__ == "__main__":
    import timeit
    s = Solution()
    nums = [1]
    print(len(nums))
    s_t = timeit.default_timer()
    print(s.rob(nums))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
