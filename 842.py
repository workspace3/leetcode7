"""
给定一个数字字符串 S，比如 S = "123456579"，我们可以将它分成斐波那契式的序列 [123, 456, 579]。

形式上，斐波那契式序列是一个非负整数列表 F，且满足：

0 <= F[i] <= 2^31 - 1，（也就是说，每个整数都符合 32 位有符号整数类型）；
F.length >= 3；
对于所有的0 <= i < F.length - 2，都有 F[i] + F[i+1] = F[i+2] 成立。
另外，请注意，将字符串拆分成小块时，每个块的数字一定不要以零开头，除非这个块是数字 0 本身。

返回从 S 拆分出来的所有斐波那契式的序列块，如果不能拆分则返回 []。

示例 1：

输入："123456579"
输出：[123,456,579]
示例 2：

输入: "11235813"
输出: [1,1,2,3,5,8,13]
示例 3：

输入: "112358130"
输出: []
解释: 这项任务无法完成。
示例 4：

输入："0123"
输出：[]
解释：每个块的数字不能以零开头，因此 "01"，"2"，"3" 不是有效答案。
示例 5：

输入: "1101111"
输出: [110, 1, 111]
解释: 输出 [11,0,11,11] 也同样被接受。
提示：

1 <= S.length <= 200
字符串 S 中只含有数字。

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/split-array-into-fibonacci-sequence
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
"""


class Solution:
    def splitIntoFibonacci(self, S: str):
        result = []
        length = len(S)
        for i in range(1, length//2+2):
            for j in range(i+1, i+(length-i)//2+2):
                if "0" == S[0] and 1 != i:
                    break
                if "0" == S[i] and j-i > 1:
                    break
                first_num = int(S[:i])
                second_num = int(S[i:j])
                curr_result = [first_num, second_num]
                curr_pos = j
                while True:
                    next_num = curr_result[-2] + curr_result[-1]
                    if S[curr_pos:].startswith(str(next_num)):
                        curr_result.append(next_num)
                        curr_pos = curr_pos + len(str(next_num))
                        if curr_pos == length:
                            result.append(curr_result)
                            break
                        elif curr_pos > length:
                            break
                    else:
                        break
        return result


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = "539834657215398346785398346991079669377161950407626991734534318677529701785098211336528511"
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.splitIntoFibonacci(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))

