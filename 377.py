# -*- coding: utf-8 -*-
"""
Given an integer array with all positive numbers and no duplicates,
find the number of possible combinations that add up to a positive integer target.

Example:

nums = [1, 2, 3]
target = 4

The possible combination ways are:
(1, 1, 1, 1)
(1, 1, 2)
(1, 2, 1)
(1, 3)
(2, 1, 1)
(2, 2)
(3, 1)

Note that different sequences are counted as different combinations.

Therefore the output is 7.
"""
from typing import List


class Solution:
    def combinationSum4(self, nums: List[int], target: int) -> int:
        # 动态规划
        dp = [0] * (target + 1)
        dp[0] = 1
        for i in range(1, len(dp)):
            tmp = 0
            for n in nums:
                if i >= n:
                    tmp += dp[i-n]
            dp[i] = tmp
        return dp[-1]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [1, 2, 3]
    target = 4
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.combinationSum4(ss, target))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
