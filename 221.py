# -*- coding: utf-8 -*-
"""
Given a 2D binary matrix filled with 0's and 1's,
find the largest square containing only 1's and return its area.

Example:

Input:

1 0 1 0 0
1 0 1 1 1
1 1 1 1 1
1 0 0 1 0

Output: 4
"""
from typing import List


class Solution:
    def maximalSquare(self, matrix: List[List[str]]) -> int:
        rows = len(matrix)
        if 0 == rows:
            return 0
        cols = len(matrix[0])
        if 0 == cols:
            return 0
        dp = []
        for row in matrix:
            dp.append([int(x) for x in row])
        for r in range(1, rows):
            for c in range(1, cols):
                if '1' == matrix[r][c]:
                    dp[r][c] = min(dp[r-1][c-1], dp[r][c-1], dp[r-1][c]) + 1
                else:
                    dp[r][c] = 0
        return max([max(item) for item in dp])**2


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [
        [1, 1, 1, 1, 0],
        [1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1],
        [1, 0, 0, 1, 0]
    ]
    new_ss = []
    for row in ss:
        new_ss.append([str(x) for x in row])
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.maximalSquare(new_ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))


