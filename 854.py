"""
Strings A and B are K-similar (for some non-negative integer K)
if we can swap the positions of two letters in A exactly K times so that the resulting string equals B.

Given two anagrams A and B, return the smallest K for which A and B are K-similar.

Example 1:

Input: A = "ab", B = "ba"
Output: 1
Example 2:

Input: A = "abc", B = "bca"
Output: 2
Example 3:

Input: A = "abac", B = "baca"
Output: 2
"""


class Solution:
    def kSimilarity(self, A: str, B: str) -> int:

        return 0


if __name__ == "__main__":
    import timeit
    s = Solution()
    A = "abc"
    B = "bca"
    s_t = timeit.default_timer()
    print(s.kSimilarity(A, B))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
