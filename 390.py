# -*- coding: utf-8 -*-
"""
问题描述：
-----------------------------------------------
There is a list of sorted integers from 1 to n.
Starting from left to right, remove the first number and
every other number afterward until you reach the end of the list.
Repeat the previous step again, but this time from right to left,
remove the right most number and every other number from the remaining numbers.
We keep repeating the steps again, alternating left to right and right to left,
until a single number remains.

Find the last number that remains starting with a list of length n.

Example:

Input:
n = 9,
1 2 3 4 5 6 7 8 9
2 4 6 8
2 6
6

Output:
6
--------------------------------------
来自讨论区的解题思路：
1. if n == 2 * k + 1
At this time, the sequence is 1, 2, 3, 4, ..., 2 * k, 2 * k + 1, delete it from left to right, then we have a new sequence: 2, 4, 6, ..., 2 * k, which is a sequence length of k. Notice that this sequence is indeed the same problem with smaller size!!! As a result, if we treat 2 * k as 1, 2 * k - 2 as 2, ..., 4 as k - 1, 2 as k,
that is,
2 4 6 8 ... 2*k oldid = (k - newid + 1) * 2
to
k k-1 k - 2 k - 3 .... 1 newid

so if we denote the solution for the smaller problem as f(k), then using the relationship between them we could simply obtain the result that

f(n) = (k - f(k) + 1) * 2 = (n / 2 - f(n / 2) + 1) * 2.

2. if n == 2 * k
At this time, the sequence is 1, 2, 3, 4, ..., 2 * k, delete it from left to right, then we have a new sequence: 2, 4, 6, ..., 2 * k, which is a sequence length of k. Since it is in the same format with the case 1. Still we have the same relation:

f(n) = (k - f(k) + 1) * 2 = (n / 2 - f(n / 2) + 1) * 2

As a result, for any n >= 1, we have the relation:

f(n) = (k - f(k) + 1) * 2 = (n / 2 - f(n / 2) + 1) * 2
"""


class Solution:
    def lastRemaining(self, n: int) -> int:
        nums = [x for x in range(1, n+1)]
        step = 2
        while (len(nums) > 1):
            if step > 0:
                nums = nums[1::step]
            else:
                nums = nums[-2::step][-1::-1]
            step *= -1
        return nums[0]


import timeit
s = Solution()
n = 99999
s_t = timeit.default_timer()
print(s.lastRemaining(n))
e_t = timeit.default_timer()
print("time cost: {:.5f} seconds.".format(e_t - s_t))