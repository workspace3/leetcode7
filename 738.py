# -*- coding: utf-8 -*-
"""
Given a non-negative integer N,
find the largest number that is less than or equal to N with monotone increasing digits.

(Recall that an integer has monotone increasing digits
if and only if each pair of adjacent digits x and y satisfy x <= y.)

Example 1:
Input: N = 10
Output: 9

Example 2:
Input: N = 1234
Output: 1234

Example 3:
Input: N = 332
Output: 299
Note: N is an integer in the range [0, 10^9].
"""


class Solution:
    def monotoneIncreasingDigits(self, N: int) -> int:
        numbers = [int(x) for x in list(str(N))]
        length = len(numbers)
        # find the first down
        pos = -1
        for i in range(length-1):
            if numbers[i] > numbers[i+1]:
                pos = i
                break
        #
        if pos < 0:
            return N
        else:
            if 1 == numbers[pos]:
                return int('9'*(length-1))
            else:
                update_pos = pos
                is_found = False
                for j in range(1, pos+1)[::-1]:
                    if numbers[j] - 1 >= numbers[j-1]:
                        update_pos = j
                        is_found = True
                        break
                if not is_found:
                    update_pos = 0
                numbers[update_pos] -= 1
                numbers = [str(numbers[k]) if k <= update_pos else '9' for k in range(length)]
                return int("".join(numbers))


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = 110000
    s_t = timeit.default_timer()
    print(s.monotoneIncreasingDigits(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
