# -*- coding: utf-8 -*-
"""
In the "100 game," two players take turns adding, to a running total, any integer from 1..10.
The player who first causes the running total to reach or exceed 100 wins.

What if we change the game so that players cannot re-use integers?

For example, two players might take turns drawing from a common pool of numbers of 1..15
without replacement until they reach a total >= 100.

Given an integer maxChoosableInteger and another integer desiredTotal,
determine if the first player to move can force a win, assuming both players play optimally.

You can always assume that maxChoosableInteger will not be larger than 20
and desiredTotal will not be larger than 300.

Example

Input:
maxChoosableInteger = 10
desiredTotal = 11

Output:
false

Explanation:
No matter which integer the first player choose, the first player will lose.
The first player can choose an integer from 1 up to 10.
If the first player choose 1, the second player can only choose integers from 2 up to 10.
The second player will win by choosing 10 and get a total = 11, which is >= desiredTotal.
Same with other integers chosen by the first player, the second player will always win.
"""


class Solution:
    def canIWin(self, maxChoosableInteger: int, desiredTotal: int) -> bool:
        candidates = [i for i in range(1, maxChoosableInteger + 1)]
        if maxChoosableInteger >= desiredTotal:
            return True
        elif sum(candidates) < desiredTotal:
            return False
        else:
            # used = [0 for _ in range(len(candidates))]
            memo = dict()

            def helper(nums, target):
                key = "{}_{}".format(nums, target)
                if key in memo:
                    return memo[key]
                elif max(nums) >= target:
                    memo[key] = True
                    return True
                else:
                    for n in nums:
                        new_nums = [xx for xx in nums if xx != n]
                        new_target = target - n
                        if not helper(new_nums, new_target):
                            memo[key] = True
                            return True
                    memo[key] = False
                    return False
            #
            return helper(candidates, desiredTotal)


if __name__ == "__main__":
    import timeit
    s = Solution()
    maxChoosableInteger = 10
    desiredTotal = 21
    s_t = timeit.default_timer()
    print(s.canIWin(maxChoosableInteger, desiredTotal))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
