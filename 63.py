# -*- coding: utf-8 -*-
"""
A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).

The robot can only move either down or right at any point in time.
The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).

Now consider if some obstacles are added to the grids. How many unique paths would there be?

An obstacle and empty space is marked as 1 and 0 respectively in the grid.

Note: m and n will be at most 100.

Example 1:

Input:
[
  [0,0,0],
  [0,1,0],
  [0,0,0]
]
Output: 2
Explanation:
There is one obstacle in the middle of the 3x3 grid above.
There are two ways to reach the bottom-right corner:
1. Right -> Right -> Down -> Down
2. Down -> Down -> Right -> Right
"""
from typing import List


class Solution:
    def uniquePathsWithObstacles(self, obstacleGrid: List[List[int]]) -> int:
        m = len(obstacleGrid)
        if 0 == m:
            return 0
        n = len(obstacleGrid[0])
        if 0 == n:
            return 0
        if 1 == obstacleGrid[0][0]:
            return 0
        dp = [[0]*n for _ in range(m)]
        for r in range(m):
            if 1 == obstacleGrid[r][0]:
                for next in range(r, m):
                    dp[r][0] = 0
                break
            else:
                dp[r][0] = 1
        for c in range(n):
            if 1 == obstacleGrid[0][c]:
                for next in range(c, n):
                    dp[0][c] = 0
                break
            else:
                dp[0][c] = 1
        #
        for r in range(1, m):
            for c in range(1, n):
                if 1 == obstacleGrid[r][c]:
                    dp[r][c] = 0
                else:
                    dp[r][c] = dp[r-1][c] + dp[r][c-1]
        return dp[m-1][n-1]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [
        [0,0,0],
        [0,1,0],
        [0,0,0]
    ]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.uniquePathsWithObstacles(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
