# -*- coding: utf-8 -*-
"""
Given an array equations of strings that represent relationships between variables,
each string equations[i] has length 4 and takes one of two different forms: "a==b" or "a!=b".
Here, a and b are lowercase letters (not necessarily different) that represent one-letter variable names.

Return true if and only if it is possible to assign integers to variable names so as to satisfy all the given equations.



Example 1:

Input: ["a==b","b!=a"]
Output: false
Explanation: If we assign say, a = 1 and b = 1, then the first equation is satisfied, but not the second.
There is no way to assign the variables to satisfy both equations.

Example 2:

Input: ["b==a","a==b"]
Output: true
Explanation: We could assign a = 1 and b = 1 to satisfy both equations.

Example 3:

Input: ["a==b","b==c","a==c"]
Output: true

Example 4:

Input: ["a==b","b!=c","c==a"]
Output: false

Example 5:

Input: ["c==c","b==d","x!=z"]
Output: true



Note:

    1 <= equations.length <= 500
    equations[i].length == 4
    equations[i][0] and equations[i][3] are lowercase letters
    equations[i][1] is either '=' or '!'
    equations[i][2] is '='
--------------------------------------------------
The problem is to find if the set of equations is satisfied.
The idea of using union find is to unit variables in '==' equation and
check if '!=' equation contradict the union we have before.
First we sort the equations in order('==' equation first and then '!=' equation ).
Then set union from variables in '==' equation using uf.union()
and last check if it is correct in '!=' equations using uf.find()

class Solution:
    def equationsPossible(self, equations: List[str]) -> bool:
        class UF: #classic union find
            def __init__(self):
                self.p={a:a for a in string.ascii_lowercase}

            def union(self,x,y):
                i=self.find(x)
                j=self.find(y)

                if i!=j:
                    self.p[j]=self.p[i]

            def find(self,x):
                if x!=self.p[x]:
                    self.p[x]=self.find(self.p[x])
                return self.p[x]

        uf=UF()
        equations.sort(key=lambda x:x[1],reverse=True)
        #sort equations,put '!=' at the end of list,deal with '==' first, once found a contrary equation return false.
        for x,e,_,y in equations:
            if e=='=':
                uf.union(x,y)
            else:
                if uf.find(x)==uf.find(y): #found contrary
                    return False
        return True

"""
from typing import List


class Solution:
    def equationsPossible(self, equations: List[str]) -> bool:
        equal, non_equal = [], []
        for item in equations:
            if item[0] != item[-1]:
                if "!" == item[1]:
                    non_equal.append(item)
                else:
                    equal.append(item)
            else:
                if "!" == item[1]:
                    return False
                else:
                    pass
        #



if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = ["a==b","e==c","b==c","a!=e"]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.equationsPossible(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
