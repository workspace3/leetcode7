# -*- coding: utf-8 -*-
"""
Given an array nums of n integers and an integer target,
find three integers in nums such that the sum is closest to target.
Return the sum of the three integers.
You may assume that each input would have exactly one solution.

Example:

Given array nums = [-1, 2, 1, -4], and target = 1.

The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
"""
from typing import List


class Solution:
    def threeSumClosest(self, nums: List[int], target: int) -> int:
        nums.sort()
        closest_distance = float('inf')
        closest_sum = None
        for i in range(len(nums) - 2):
            left, right = i + 1, len(nums) - 1
            left_sum = nums[i] + nums[left] + nums[left+1]
            right_sum = nums[i] + nums[right] + nums[right-1]
            if left_sum > target:
                curr_distance = abs(left_sum - target)
                if curr_distance < closest_distance:
                    closest_sum = left_sum
                    closest_distance = curr_distance
            elif right_sum < target:
                curr_distance = abs(right_sum - target)
                if curr_distance < closest_distance:
                    closest_sum = right_sum
                    closest_distance = curr_distance
            else:
                while left < right:
                    curr_sum = nums[i] + nums[left] + nums[right]
                    if curr_sum == target:
                        return curr_sum
                    elif curr_sum > target:
                        right -= 1
                    else:
                        left += 1
                    curr_distance = abs(curr_sum - target)
                    if curr_distance < closest_distance:
                        closest_sum = curr_sum
                        closest_distance = curr_distance
        return closest_sum


if __name__ == "__main__":
    import timeit
    s = Solution()
    # nums = [82,-16,-60,-48,32,-30,0,-89,70,-46,-82,-58,66,41,-96,-55,-49,-87,-33,-65,9,14,81,-11,80,-93,79,-63,-61,-16,22,-31,75,12,17,-6,37,-2,-89,-66,3,-95,-74,58,-95,22,11,-20,-36,60,-75,46,-52,-61,-28,7,-50,-45,93,-91,-43,35,-99,-39,53,-54,-98,-4,13,-90,23,-4,-65,29,85,-76,-64,81,32,-97,51,12,-82,-31,81,-2,-83,-9,89,-37,-23,-66,-91,-15,-98,-74,94,30,-2,-70,13,19,-77,-42,33,-70,25,77,47,-70,-70,60,-63,-4,83,13,-78,-23,28,-86,-11,-16,-57,-84,51,-48,-63,-15,29,56,-25,73,-69,23,28,90,96,41,65,-22,-43,-68,-77,31,69,-84,23,-63,-18,20,-93,-17,-48,-73,14,-95,98,-64,-12,-45,14,7,51,-61,89,-48,-23,2,-56,84,-2,27,74,-39,-18,-65,79,-36,-76,-30,44,78,-76,37,88,0,32,55,-51,23,-9,68,26,15,66,66,-56,-42,56,28,33,-32,-23,-36,-12,-76,-12,42,12,40,69,54,82,-22,-7,46,-46]
    nums = [-1,0,1,1,55]
    target = 3
    s_t = timeit.default_timer()
    print(s.threeSumClosest(nums, target))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
