# -*- coding: utf-8 -*-
"""
Given a binary array, find the maximum length of a contiguous subarray with equal number of 0 and 1.

Example 1:
Input: [0,1]
Output: 2
Explanation: [0, 1] is the longest contiguous subarray with equal number of 0 and 1.
Example 2:
Input: [0,1,0]
Output: 2
Explanation: [0, 1] (or [1, 0]) is a longest contiguous subarray with equal number of 0 and 1.
Note: The length of the given binary array will not exceed 50,000.

"""
from typing import List


class Solution:
    def findMaxLength(self, nums: List[int]) -> int:
        record = {0: [0]}
        result = 0
        for i in range(1, len(nums)+1):
            if 1 == nums[i-1]:
                result += 1
            else:
                result -= 1
            if result not in record:
                record[result] = [i]
            else:
                record[result].append(i)
        candidates = [record[k][-1]-record[k][0] for k in record if len(record[k]) > 1]
        if len(candidates) == 0:
            return 0
        else:
            return max(candidates)


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [0, 1, 0, 1, 1, 0, 0]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.findMaxLength(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
