# -*- coding: utf-8 -*-
"""
Given a m x n grid filled with non-negative numbers,
find a path from top left to bottom right which minimizes the sum of all numbers along its path.

Note: You can only move either down or right at any point in time.

Example:

Input:
[
  [1,3,1],
  [1,5,1],
  [4,2,1]
]
Output: 7
Explanation: Because the path 1→3→1→1→1 minimizes the sum.
"""
from typing import List


class Solution:
    def minPathSum(self, grid: List[List[int]]) -> int:
        m = len(grid)
        if 0 == m:
            return 0
        n = len(grid[0])
        if 0 == n:
            return 0
        dp = [[0]*n for _ in range(m)]
        dp[0][0] = grid[0][0]
        for r in range(1, m):
            dp[r][0] = dp[r-1][0] + grid[r][0]
        for c in range(1, n):
            dp[0][c] = dp[0][c-1] + grid[0][c]
        #
        for r in range(1, m):
            for c in range(1, n):
                dp[r][c] = min(dp[r-1][c], dp[r][c-1]) + grid[r][c]
        return dp[m-1][n-1]


if __name__ == "__main__":
    import timeit
    s = Solution()
    ss = [
        [1,3,1],
        [1,5,1],
        [4,2,1]
    ]
    print(len(ss))
    s_t = timeit.default_timer()
    print(s.minPathSum(ss))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
