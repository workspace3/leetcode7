# -*- coding: utf-8 -*-
"""
Given a positive integer n, find the least number of perfect square numbers
(for example, 1, 4, 9, 16, ...) which sum to n.

Example 1:

Input: n = 12
Output: 3
Explanation: 12 = 4 + 4 + 4.
Example 2:

Input: n = 13
Output: 2
Explanation: 13 = 4 + 9.
"""
import math


class Solution:
    def numSquares(self, n: int) -> int:
        if 1 == n:
            return 1
        squares = [x**2 for x in range(int(math.sqrt(n))+1)]
        level = 0
        curr_nodes = [0]
        next_nodes = set()
        while True:
            level += 1
            for upper_node in curr_nodes:
                for node in squares:
                    _sum = upper_node + node
                    if _sum == n:
                        return level
                    elif _sum < n:
                        next_nodes.add(_sum)
            curr_nodes = list(next_nodes)
            next_nodes = set()


if __name__ == "__main__":
    import timeit
    s = Solution()
    nums = 13
    s_t = timeit.default_timer()
    print(s.numSquares(nums))
    e_t = timeit.default_timer()
    print("time cost: {:.5f} seconds.".format(e_t - s_t))
